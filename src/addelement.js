import {extend} from './object.js';

// Insert a <script> element asynchronously
export const addScript = function addScript(url, sid, callback) {
  let loadScript = document.createElement('script');
  let script0 = document.getElementsByTagName('script')[0];
  let done = false;

  loadScript.async = 'async';
  loadScript.src = url;

  // In case someone puts the callback in the 2nd arg.
  if (typeof sid === 'function') {
    callback = sid;
    sid = null;
  }
  sid = sid || `s-${new Date().getTime()}`;

  // If there's a callback, set handler to call it after the script loads
  // NOTE: script may not be parsed by the time callback is called.
  if (callback) {
    loadScript.onload = loadScript.onreadystatechange = function() {
      if (!done &&
          (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')
      ) {
        done = true;
        callback();
        loadScript.onload = loadScript.onreadystatechange = null;
        script0.parentNode.removeChild(loadScript);
      }

    };
  }

  if (!document.getElementById(sid)) {
    script0.parentNode.insertBefore(loadScript, script0);
  }
};

// Insert a CSS <link> element in the head.
export const addLink = function addLink(params) {
  let h = document.getElementsByTagName('head')[0];
  let opts = extend({
    media: 'screen',
    rel: 'stylesheet',
    type: 'text/css',
    href: '',
  }, params);

  // bail out if the <link> element is already there
  for (let i = 0, lnks = h.getElementsByTagName('link'), ll = lnks.length; i < ll; i++) {
    if (!opts.href || lnks[i].href.indexOf(opts.href) !== -1) {
      return;
    }
  }
  let lnk = document.createElement('link');

  for (let prop in opts) {
    lnk[prop] = opts[prop];
  }
  h.appendChild(lnk);

  lnk = null;
};
