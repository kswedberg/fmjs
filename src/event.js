/**
 * @module event
 * @summary
 * ES6 Import Example:
 * ```js
 * import {addEvent} from 'fmjs';
 *
 * // or:
 * import {addEvent} from 'fmjs/event.js';
 * ```
 *
 */

const listener = {
  prefix: '',
};

let FM = typeof window !== 'undefined' && window.FM || {};

FM.windowLoaded = typeof document !== 'undefined' && document.readyState === 'complete';

// See passive test: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
let supportsObject = false;

try {
  const options = {
    // This function is called when the browser attempts to access the capture property
    get capture() {
      supportsObject = true;

      return false;
    },
  };

  window.addEventListener('test', null, options);
  window.removeEventListener('test', null, options);
} catch (err) {
  supportsObject = false;
}

let hasAttachEvent = function() {
  const doc = typeof window !== 'undefined' && window.document || {};

  return doc.attachEvent === 'function' || typeof doc.attachEvent == 'object';
};

if (typeof addEventListener === 'function') {
  listener.type = 'addEventListener';
} else if (hasAttachEvent()) {
  listener.type = 'attachEvent';
  listener.prefix = 'on';
} else {
  listener.prefix = 'on';
}

const normalizeOptions = (options) => {
  return typeof options === 'object' ? options : {
    capture: !!options.capture,
  };
};
let removeEvent;

/**
 * A wrapper around `addEventListener` that deals with browser inconsistencies (e.g. `capture`, `passive`, `once` props on `options` param; see param documentation below for details) and handles window load similar to how jQuery handles document ready by triggering  handler immediately if called *after* the event has already fired.
 * For triggering window load, this file MUST be imported before window.load occurs.
 * @function addEvent
 * @param {Element} el DOM element to which to attach the event handler
 * @param {string} type Event type
 * @param {function} handler(event) Handler function. Takes `event` as its argument
 * @param {Object|boolean} [options = false] Optional object or boolean. If boolean, indicates whether the event should be in "capture mode" rather than starting from innermost element and bubbling out. Default is `false`. If object, and browser does not support object, argument is set to capture property if provided
 * @param {boolean} [options.capture = false] Indicates if the event should be in "capture mode" rather than starting from innermost element and bubbling out. Default is `false`.
 * @param {boolean} [options.passive] If `true`, uses passive mode to reduce jank. **This is automatically set to `true`** for supported browsers if not explicitly set to `false` for the following event types: touchstart, touchmove, scroll, wheel, mousewheel. Ignored if not supported.
 * @param {boolean} [options.once] If `true`, removes listener after it is triggered once on the element.
*/
export let addEvent = function(el, type, fn, options = false) {
  // Call immediately if window already loaded and calling addEvent(window, 'load', fn)
  if (FM.windowLoaded && type === 'load' && el === window) {
    return fn.call(window, {windowLoaded: true, type: 'load', target: window});
  }
  const opts = normalizeOptions(options);

  if (!supportsObject) {
    const fun = !opts.once ? fn : function(event) {
      fn(event);
      removeEvent(type, fun, opts.capture);
    };

    return el[ listener.type ](listener.prefix + type, fun, opts.capture);
  }

  const passiveEvents = ['touchstart', 'touchmove', 'scroll', 'wheel', 'mousewheel'];

  if (passiveEvents.indexOf(type) !== -1 && opts.passive !== false) {
    opts.passive = true;
  }

  return el[listener.type] && el[listener.type](listener.prefix + type, fn, opts);
};

// Modify addEvent for REALLY OLD browsers that have neither addEventListener nor attachEvent
if (!listener.type) {
  addEvent = function(el, type, fn) {
    el[listener.prefix + type] = fn;
  };
}

/**
* A wrapper around `removeEventListener` that naïvely deals with oldIE inconsistency.
* @function removeEvent
* @param {Element} el DOM element to which to attach the event handler
* @param {string} type Event type.
* @param {function} [handler] Handler function to remove.
* @param {Object|boolean} [options = false] Optional object or boolean. If boolean, indicates whether event to be removed was added in "capture mode". Important: non-capturing here only removes non-capturing added event and vice-versa.
* @param {boolean} [options.capture] Indicates whether event to be removed was added in "capture mode"
*/
removeEvent = function(el, type, fn, options = false) {
  const listenerType = typeof window.removeEventListener === 'function' ? 'removeEventListener' : document.detachEvent && 'detachEvent';
  const opts = normalizeOptions(options);

  if (listenerType) {
    return el[ listenerType ](listener.prefix + type, fn, opts.capture);
  }

  el[listener.prefix + type] = null;
};

export {removeEvent};

if (typeof window !== 'undefined') {
  window.FM = Object.assign(window.FM || {}, FM);

  // call addEvent on window load
  if (!FM.windowLoaded) {
    addEvent(window, 'load', () => {
      FM.windowLoaded = true;
      if (window.FM) {
        window.FM.windowLoaded = true;
      }
    });
  }
}
