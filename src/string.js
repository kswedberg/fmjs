/**
 * @module string
 * @summary ES6 Import Example:
 * ```js
 * import {slugify} from 'fmjs';
 *
 * // or:
 * import {slugify} from 'fmjs/string.js';
 * ```
 *
 * CommonJS Require Example:
 * ```js
 * const {slugify} = require('fmjs/cjs/string.js');
 * ```
 *
 */

import {extend} from './object.js';

/**
 * @function
 * @param {string} str Word to pluralize
 * @param {number} num Number of items
 * @param {string} [ending = s] Optional ending of the pluralized word
 * @returns {string} Pluralized string
 */
export const pluralize = function pluralize(str, num, ending) {
  num = num * 1;

  if (ending === undefined) {
    ending = 's';
  }

  if (num !== 1) {
    str += ending;
  }

  return str;
};

const capWord = (word) => {
  const keepLower = ['and', 'or', 'a', 'an', 'the', 'her', 'his', 'its', 'our', 'your', 'their', 'about', 'above', 'across', 'after', 'against', 'along', 'among', 'around', 'at', 'before', 'behind', 'between', 'beyond', 'but', 'by', 'concerning', 'despite', 'down', 'during', 'except', 'following', 'for', 'from', 'in', 'including', 'into', 'like', 'near', 'of', 'off', 'on', 'out', 'over', 'since', 'through', 'throughout', 'to', 'towards', 'under', 'until', 'up', 'upon', 'with', 'within', 'without'];
  const rBaseWord = /^([a-zA-Z]+).*$/;
  const baseWord = word.replace(rBaseWord, '$1');

  if (keepLower.includes(baseWord)) {
    return word;
  }

  return word.slice(0, 1).toUpperCase() + word.slice(1);
};

const caseChanges = {
  sentence: (str) => {
    if (!str) {
      return '';
    }

    return str.slice(0, 1).toUpperCase() + str.slice(1);
  },

  title: (str) => {

    const words = (str || '').split(/\s+/);
    const firstWord = caseChanges.sentence(words.shift());

    const moreWords = words.map(capWord);

    return [firstWord, ...moreWords].join(' ');
  },

  caps: (str) => {
    return (str || '').toUpperCase();
  },

  camel: (str) => {
    if (!str) {
      return '';
    }

    return str
    // Lowercase the whole thing
    .toLowerCase()
    // Split on anything Not alphanumeric
    .split(/[^a-z0-9]+/)
    // Make all but first one initial cap
    .map((item, i) => {
      return i ? item.slice(0, 1).toUpperCase() + item.slice(1) : item;
    })
    // Reassemble
    .join('');
  },

  slug: (str) => {
    return (str || '')
    .toLowerCase()
    .replace(/[^a-z0-9]+/g, '-')
    // replace repeating dashes with a single dash
    .replace(/--+/g, '-')
    // remove leading and trailing dashes
    .replace(/^-|-$/g, '') || str;
  },
};

/**
 * Changes the case of the provided words according to the `type`.
 * @function
 * @param {string} str String that will be cased as determined by `type`
 * @param {string} type One of 'title|sentence|caps|camel|slug'
 * @returns {string} Converted string
 * @example
 * const oldMan = 'the old man and the sea';
 *
 *console.log(changeCase(oldMan, 'title'));
 * // Logs: 'The Old Man and the Sea'
 *
 * console.log(changeCase(oldMan, 'sentence'));
 * // Logs: 'The old man and the sea'
 *
 * console.log(changeCase(oldMan, 'camel'));
 * // Logs: 'theOldManAndTheSea'
 */
export const changeCase = (str, type) => {
  if (!caseChanges[type]) {
    return str;
  }

  return caseChanges[type](str);
};

/**
 * Slugify a string by lowercasing it and replacing white spaces and non-alphanumerics with dashes.
 * @function
 * @param {string} str String to be converted to a slug
 * @returns {string} "Slugified" string
 * @example
 * console.log(slugify('Hello there, how are you?'));
 * // Logs: 'hello-there-how-are-you'
 *
 * console.log(slugify('  You? & Me<3* '));
 * // Logs: 'you-me-3'
 */
export const slugify = function slugify(str) {
  return caseChanges.slug(str);
};

/**
 * Add commas (or provided `separator`) to a number, or a string representing a number, of 1000 or greater.
 * @function
 * @param {string|number} val Number to be formatted as a string
 * @param {string} [separator = ,] punctuation to be used for thousands, millions, etc
 * @returns  {string} number formatted as string
 */
export const commafy = function commafy(val, separator = ',') {
  val = val * 1;

  let stringVal = `${val}`.split('.');

  if (val >= 1000) {
    let int = '';
    let tmp = stringVal[0].split('');

    for (let i = tmp.length - 1, j = 0; i >= 0; i--) {
      int = tmp[i] + int;

      if (j++ % 3 === 2 && i) {
        int = `${separator}${int}`;
      }
    }
    stringVal[0] = int;
  }


  return stringVal.join('.');
};

export const decimalify = function decimalify(decimal, places) {
  let i;

  let dec = decimal || '';

  if (dec.length === places) {
    return dec;
  }

  if (places == null) {
    return dec;
  }

  if (dec.length < places) {
    // If fewer decimal places than needed, "right-pad" with zeros
    for (i = dec.length; i < places; i++) {
      dec += '0';
    }
  } else if (dec.length > places) {
    // eslint-disable-next-line no-restricted-properties
    dec = Math.round((dec * 1) / Math.pow(10, dec.length - places));
    dec = `${dec}`;

    // After rounding to the proper level, need to "left-pad" to match number of decimal places
    // For example, rounding to 2 decimal places, 0472323 will change to 05, which will be read only as 5
    for (i = dec.length; i < places; i++) {
      dec = `0${dec}`;
    }
  }

  return dec;
};

// Convert a number to a formatted string
export const formatNumber = function formatNumber(val, options) {
  options = options || {};
  let numParts = [];
  let num = '';
  let output = '';
  let opts = extend({
    prefix: '',
    suffix: '',
    // decimalPlaces: undefined
    includeHtml: typeof options.prefix === 'string' && options.prefix.indexOf('$') !== -1,
    classRoot: 'Price',
  }, options);

  let cr = opts.classRoot;

  num = `${val}`;
  numParts = num.split('.');

  numParts[1] = decimalify(numParts[1], opts.decimalPlaces);

  numParts[0] = commafy(numParts[0]);

  if (opts.decimalPlaces === 0) {
    numParts = numParts.slice(0, 1);
  }

  if (opts.includeHtml) {
    output = `<span class="${cr}"><span class="${cr}-prefix">${opts.prefix}</span>`;
    output += `<span class="${cr}-int">${numParts[0]}</span>`;

    if (opts.decimalPlaces !== 0) {
      output += `<span class="${cr}-dec">.</span>`;
      output += `<span class="${cr}-decNum">${numParts[1]}</span>`;
    }

    output += opts.suffix ? `<span class="${cr}-suffix">${opts.suffix}</span>` : '';
    output += '</span>';
  } else {
    output = opts.prefix + numParts.join('.') + opts.suffix;
  }

  return output;
};

/**
 * ROT13 encode/decode a string
 * @function
 * @param {string} string String to be converted to or from ROT13
 * @returns {string} The encoded (or decoded) string
 */
export const rot13 = function rot13(string) {
  const str = string.replace(/[a-zA-Z]/g, (c) => {
    let cplus = c.charCodeAt(0) + 13;

    return String.fromCharCode((c <= 'Z' ? 90 : 122) >= cplus ? cplus : cplus - 26);
  });

  return str;
};

/**
 * Convert a string to Java-like numeric hash code
 * @function
 * @param {string} str String to be converted
 * @param {string} [prefix] Optional prefix to the hash
 * @returns {number|string} The converted hash code as numeral (or string, if prefix is provided)
 * @see http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
 */
export const hashCode = function hashCode(str, prefix) {
  let hash = 0;
  let chr;

  if (str.length === 0) {
    return hash;
  }

  for (let i = 0, len = str.length; i < len; i++) {
    chr = str.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    // Convert to 32bit integer
    hash |= 0;
  }

  hash = hash >= 0 ? hash : hash * -1;

  if (prefix) {
    hash = prefix + hash;
  }

  return hash;
};

/**
 * Return a base64-encoded string based on the provided string.
 * If the browser does not support this type of encoding, returns the string unchanged.
 * @function
 * @param {string} str String to be base4 encoded
 * @returns {string} base64-encoded string
 */
export const base64Encode = function base64Encode(str) {
  if (typeof btoa === 'undefined') {
    return str;
  }

  return btoa(encodeURIComponent(str));
};

/**
* Return a decoded string based on the provided base64-encoded string.
* If the browser does not support this type of encoding, returns the string unchanged.
* @function
* @param {string} str base4-encoded string
* @returns {string} decoded string
*/
export const base64Decode = function base64Decode(str) {
  if (typeof atob === 'undefined') {
    return str;
  }

  return decodeURIComponent(atob(str));
};
