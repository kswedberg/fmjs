/**
 * @module promise
 * @summary ES6 Import Example:
 * ```js
 * import {peach} from 'fmjs';
 *
 * // or:
 * import {peach} from 'fmjs/promise.js';
 * ```
 *
 * CommonJS Require Example:
 * ```js
 * const {peach} = require('fmjs/cjs/promise.js');
 * ```
 *
 */

/**
 * "Promised `each()`" for iterating over an array of items, calling a function that returns a promise for each one. So, each one waits for the previous one to resolve before being called
 * @function peach
 * @param {array} arr Array to iterate over
 * @param {callback} callback(item,i) Function that is called for each element in the array, each returning a promise
 * @returns {array.<Promise>} Array of promises
 */
export const peach = (arr, fn) => {
  const funcs = arr.map((item, i) => {
    return () => fn(item, i);
  });

  return funcs.reduce((promise, func) => {
    return promise
    .then((result) => {
      const called = func();
      // If the function doesn't return a "then-able", create one with Promise.resolve():

      return (typeof called.then === 'function' ? called : Promise.resolve(called))
      .then([].concat.bind(result));
    });

  }, Promise.resolve([]));
};
