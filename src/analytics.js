/* eslint-disable no-underscore-dangle */
/**
 * @module analytics
 * @summary
 * ES6 Import Example:
 * ```js
 * import {analytics} from 'fmjs';
 *
 * // or:
 * import {analytics} from 'fmjs/analytics.js';
 * ```
 *
 */

/**
 * Load the google analytics script and set it up to track page views. If the document title has "page not found" in it (case insensitive). It'll prepend `/404/` to the url for the page-view tracking.

 * @param {string} id The google analytics ID
 * @param {string} [type] The only possible value for the `type` argument is `'legacy'`.
 * @warning untested
 */
export const analytics = function(id, type) {
  let FM = window.FM || {};

  let url = type === 'legacy' ? 'https://ssl.google-analytics.com/ga.js' : 'https://www.google-analytics.com/analytics.js';

  let noId = !id || id.indexOf('XXXXX') !== -1;

  if (noId || document.getElementById(id)) {
    console.log(noId ? 'No google analytics id.' : 'GA script already loaded.');

    return;
  }

  let trackPageview = FM.trackPageview || ['_trackPageview'];
  let _gaq = window._gaq;

  if (type === 'legacy') {
    if (trackPageview.length === 1 && (/page not found/i).test(document.title)) {
      trackPageview.push(`/404/${window.location.pathname.replace(/^\//, '')}`);
    }

    _gaq = [
      ['_setAccount', id],
      // add site-specific parameters here.
      // ['_setDomainName', 'none'],
      // ['_setAllowLinker', true],
      // ['_setAllowHash', false],

      // finish up
      trackPageview,
    ];
  }

  (function(win, doc) {
    let scriptNew, script0;

    if (type === 'legacy') {
      window._gaq = _gaq;
      window.ga = function() {/* intentionally empty */};
    } else {
      win.GoogleAnalyticsObject = 'ga';
      win.ga = win.ga || function(...args) {
        (win.ga.q = win.ga.q || []).push(args);
      };
      win.ga.l = 1 * new Date();
    }
    scriptNew = doc.createElement('script');
    script0 = doc.getElementsByTagName('script')[0];
    scriptNew.async = 1;
    scriptNew.src = url;
    scriptNew.id = id;
    script0.parentNode.insertBefore(scriptNew, script0);
  })(window, document);

  window.ga('create', id, 'auto');

  if (/page not found/i.test(document.title)) {
    window.ga('send', 'pageview', {
      page: `/404/${window.location.pathname.replace(/^\//, '')}`,
    });
  } else {
    window.ga('send', 'pageview');
  }

};
