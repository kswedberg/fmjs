// Core FM constructor. Load this first

'use strict';

import {extend, getProperty} from './object.js';

let core = {
  extend,

  // Get value of a nested property, without worrying about reference error
  getProperty,

  // Number of pixels difference between touchstart and touchend necessary
  // for a swipe gesture from fm.touchevents.js to be registered
  touchThreshold: 50,
};

// Export each core method separately
export {extend, getProperty};

export const touchThreshold = core.touchThreshold;

// Add FM with its core methods to global window object if window is available
if (typeof window !== 'undefined') {

  if (typeof window.FM === 'undefined') {
    window.FM = {};
  }

  for (let name in core) {
    let prop = core[name];

    if (typeof window.FM[name] === 'undefined') {
      window.FM[name] = prop;
    }
  }
}
