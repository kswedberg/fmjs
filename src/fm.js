import * as core from './core';

const FM = typeof window !== 'undefined' && window.FM ? Object.assign(window.FM, core) : core;

import * as addElements from './addelement.js';
import * as ajaxes from './ajax.js';
import * as ga from './analytics.js';
import * as arrays from './array.js';

import * as events from './event.js';
import * as forms from './form.js';
import * as jsonp from './jsonp.js';
import * as maths from './math.js';
import * as objects from './object.js';
import * as promises from './promise.js';
import * as selections from './selection.js';
import * as storages from './storage.js';
import * as strings from './string.js';
import * as timers from './timer.js';
import * as transEvents from './transition-event.js';
import * as urls from './url.js';
import * as youtubes from './youtube.js';

import * as colors from './color.js';
import * as doms from './dom.js';

FM.extend(FM, addElements, ajaxes, ga, arrays, events, forms, jsonp, maths, objects);
FM.extend(FM, promises, selections, storages, strings, timers, transEvents, urls, youtubes);

FM.extend(FM, colors, doms);

window.FM = FM;
