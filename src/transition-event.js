// http://davidwalsh.name/css-animation-callback
const detectEndName = function detectEndName(type) {
  let el = document.createElement('fake');
  let names = {
    transition: {
      transition: 'transitionend',
      WebkitTransition: 'webkitTransitionEnd',
      MozTransition: 'transitionend',
    },
    animation: {
      animation: 'animationend',
      WebkitAnimation: 'webkitAnimationEnd',
      MozAnimation: 'animationend',
    },
  };
  let testNames = names[type];

  for (let t in testNames) {
    if (el.style[t] !== undefined) {
      return testNames[t];
    }
  }
};

export let transitionEnd = detectEndName('transition');
export let animationEnd = detectEndName('animation');
