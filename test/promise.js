import {peach} from '../src/promise.js';

const assert = require('assert');

describe('Promise', () => {
  describe('peach', () => {
    // it('iterates through array', function(done) {
    //   const fns = [
    //     Promise.resolve('one'),
    //     'two',
    //     new Promise((resolve) => {
    //       setTimeout(resolve, 50, 'three');
    //     }),
    //   ];

    //   const msgs = [];

    //   peach(fns, (item) => {
    //     return msgs.push(item);
    //   })
    //   .then(() => {
    //     assert.equal(msgs.length, 3, 'All three items in array were pushed');
    //     done();
    //   });
    // });

    // it('gets last promised element correctly', function(done) {
    //   const msgs = [];

    //   peach(fns, (item) => {
    //     return msgs.push(item);
    //   })
    //   .then(() => {
    //     msgs[2]
    //     .then((value) => {
    //       assert.equal(msgs[2], 'three', 'Last promised element return value is correct');
    //       done();
    //     });

    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     done(err);
    //   });
    // });

    it('returns an array', (done) => {
      const msgs = [];
      const arr = [
        Promise.resolve('one'),
        'two',
        new Promise((resolve) => {
          setTimeout(resolve, 50, 'three');
        }),
      ];

      peach(arr, (item, i) => {
        msgs.push(item);

        return item;
      })
      .then((items) => {
        console.log(items);
        assert.equal(items.length, 3, 'Items returned as expected array');
        assert.equal(items[2], 'three', 'Last promised element return value is correct');
        done();
      })
      .catch(done);
    });
  });
});
