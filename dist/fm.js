/**
 * Fusionary JS by Karl Swedberg
 * Copyright (c) 2020 ; Licensed MIT
 */

(function () {
  'use strict';

  /**
   * @module array
   * @summary ES6 Import Example:
   * ```js
   * import {isArray} from 'fmjs';
   *
   * // or:
   * import {isArray} from 'fmjs/array.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {isArray} = require('fmjs/cjs/array.js');
   * ```
   *
   */

  /**
   * Determine whether "arr" is a true array
   * @function isArray
   * @param  {array} arr item to determine whether it's an array
   * @returns {boolean}     `true` if arr is array, `false` if not
   * @example
   * import {isArray} from 'fmjs/array.js';
   *
   * if (isArray(window.foo)) {
   *   window.foo.push('bar');
   * }
   */
  var isArray = function(arr) {
    if (Array.isArray) {
      return Array.isArray(arr);
    }

    return typeof arr === 'object' && Object.prototype.toString.call(arr) === '[object Array]';
  };

  /**
   * Determine whether item "el" is in array "arr"
   * @function inArray
   * @param  {Any} el  An item to test against the array
   * @param  {array} arr The array to test against
   * @returns {boolean}     Boolean (`true` if el is in array, `false` if not)
   */
  var inArray = function(el, arr) {
    if (arr.includes) {
      return arr.includes(el);
    }

    if (arr.indexOf) {
      return arr.indexOf(el) !== -1;
    }

    for (var i = arr.length - 1; i >= 0; i--) {
      if (arr[i] === el) {
        return true;
      }
    }

    return false;
  };

  /**
   * Return a random item from the provided array
   * @function randomItem
   * @param  {array} arr An array of elements
   * @returns {Any}     A random element from the provided array
   */
  var randomItem = function randomItem(arr) {
    var index = Math.floor(Math.random() * arr.length);

    return arr[index];
  };

  /**
   * Take an array of objects and a property and return an array of values of that property
   * @function pluck
   * @param  {array} arr  Array from which to pluck
   * @param  {string} prop Property to pluck
   * @returns {array} Array of values of the property (if the value is `undefined`, returns `null` instead)
  * @example import {pluck} from 'fmjs/array.js';
  *
  * let family = [
  *   {
  *     id: 'dad',
  *     name: 'Karl'
  *   },
  *   {
  *     id: 'mom',
  *     name: 'Sara',
  *     color: 'blue'
  *   },
  *   {
  *     id: 'son',
  *     name: 'Ben',
  *     color: 'green'
  *   },
  *   {
  *     id: 'daughter',
  *     name: 'Lucy'
  *   }
  * ];
  *
  * let names = pluck(family, 'name');
  * let ids = pluck(family, 'id');
  * let colors = pluck(family, 'color');
  *
  * console.log(names);
  * // Logs: ['Karl', 'Sara', 'Ben', 'Lucy']
  *
  * console.log(ids);
  * // Logs: ['dad', 'mom', 'son', 'daughter']
  *
  * console.log(colors);
  * // Logs: [null, 'blue', 'green', null]
   */
  var pluck = function pluck(arr, prop) {
    if ( arr === void 0 ) arr = [];

    return arr.map(function (el) {
      return typeof el[prop] === 'undefined' ? null : el[prop];
    });
  };

  /**
   * Fisher-Yates (aka Knuth) shuffle. Takes an array of elements and returns the same array, but with its elements shuffled
   * @function shuffle
   * @see [knuth-shuffle]{@link https://github.com/coolaj86/knuth-shuffle}
   * @param  {array} arr Array to be shuffled
   * @returns {array}     The array passed to `arr`, shuffled
   */
  var shuffle = function(els) {
    var temporaryValue, randomIndex;
    var currentIndex = els.length;

    // While there remain elements to shuffle...
    while (currentIndex !== 0) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = els[currentIndex];
      els[currentIndex] = els[randomIndex];
      els[randomIndex] = temporaryValue;
    }

    return els;
  };

  /**
   * Collapse two or more arrays into a single, new array. Same as `merge()`, but not limited to two arrays.
   * @function collapse
   * @param {array} array1 First array
   * @param {array} array2 Second array
   * @param  {...array} arrays Additional arrays to collapse
   * @see [merge]{@link #module_array..merge}
   * @returns {array} A new collapsed array
   * @warning untested
   */
  var collapse = function() {
    var arrays = [], len$1 = arguments.length;
    while ( len$1-- ) arrays[ len$1 ] = arguments[ len$1 ];

    var tmp = [];

    for (var i = 0, len = arrays.length; i < len; i++) {
      tmp.push.apply(tmp, arrays[i]);
    }

    return tmp;
  };

  /**
   * Merge two arrays into a single, new array. Same as `collapse()` but only works with two array arguments.
   * @function merge
   * @param {array} array1 First array
   * @param {array} array2 Second array
   * @see [collapse]{@link #module_array..collapse}
   * @returns {array} A new merged array
   * @warning untested
   */
  var merge = function(arr1, arr2) {
    return collapse(arr1, arr2);
  };

  /**
   * Return a subset of `array1`, only including elements from `array2` that are also in `array1`.
   * * If `prop` is provided, only that property of an element needs to match for the two arrays to be considered intersecting at that element
   * @function intersect
   * @param {array} arr1 First array
   * @param {array} arr2 Second array
   * @param {any} [prop] Optional property to compare in each element of the array
   * @returns {array} A new filtered array
   * @example
   * const array1 = [{name: 'Foo', id: 'a'}, {name: 'Bar', id: 'b'}];
   * const array2 = [{name: 'Foo', id: 'z'}, {name: 'Zippy', id: 'b'}];
   *
   * console.log(intersect(array1, array2, 'name'));
   * // Logs [{name: 'Foo', id: 'a'}]
   *
   * console.log(intersect(array1, array2, 'id'));
   * // Logs [{name: 'Bar', id: 'b'}]
   */
  var intersect = function(arr1, arr2, prop) {

    return arr1.filter(function (item) {
      if (typeof item !== 'object') {
        return arr2.includes(item);
      }

      // If a prop argument is provided, we only need to match on that property of the object
      if (prop) {
        return !!arr2.find(function (arr2Item) { return arr2Item[prop] === item[prop]; });
      }

      // If NO prop provided, match against the entire (stringified) object
      var strung = JSON.stringify(item);

      return !!arr2.find(function (arr2Item) { return strung === JSON.stringify(arr2Item); });
    });
  };

  /**
   * Take an array of elements and return an array containing unique elements.
   * If an element is an object or array:
   * * when `prop` is *undefined*, uses `JSON.stringify()` when checking the elements
   * * when `prop` is *provided*, only that property needs to match for the element to be considered a duplicate and thus excluded from the returned array
   * @function unique
   * @param {array} arr Array to be filtered by uniqueness of elements (or property of elements)
   * @param {Any} [prop] Optional property to be tested if an element in `arr` is an object or array
   * @returns {array} A new filtered array
   * @example
   * const array1 = [1, 2, 3, 2, 5, 1];
   * const uniq = unique(array1);

   * console.log(uniq);
   * // Logs: [1, 2, 3, 5]
   */
  var unique = function(arr, prop) {
    return arr.filter(function (item, i) {
      if (typeof item !== 'object') {
        return arr.indexOf(item) === i;
      }

      // If a prop argument is provided, we only need to match on that property of the object
      if (prop) {
        return arr.findIndex(function (test) { return test[prop] === item[prop]; }) === i;
      }

      // If NO prop provided, match against the entire (stringified) object
      var strung = JSON.stringify(item);

      return arr.findIndex(function (test) { return JSON.stringify(test) === strung; }) === i;
    });
  };

  /**
   * Return a subset of `array1`, only including elements that are NOT also in `array2`. The returned array won't include any elements from `array2`.
   * If an element is an object or array:
   * * when `prop` is *undefined*, uses `JSON.stringify()` when performing the comparison on an object or array
   * * when `prop` is *provided*, only that property needs to match for the item to be excluded fom the returned array
   * @function diff
   * @param {array} arr1 Array for which to return a subset
   * @param {array} arr2 Array to use as a comparison
   * @param {string} [prop] Optional property to be tested if an element in `arr1` is an object or array
   * @returns {array} A filtered array
   * @example
   * const array1 = [1, 2, 3, 4];
   * const array2 = [2, 3, 5, 6, -1];

   * console.log(diff(array1, array2));
   * // Logs: [1, 4]
   */
  var diff = function(arr1, arr2, prop) {
    return arr1.filter(function (item) {

      if (typeof item !== 'object') {
        return !arr2.includes(item);
      }

      if (prop) {
        return !arr2.find(function (arr2Item) { return item[prop] === arr2Item[prop]; });
      }

      var strung = JSON.stringify(item);

      return !arr2.find(function (arr2Item) { return strung === JSON.stringify(arr2Item); });
    });
  };

  /**
   * From an array passed into the first argument, create an array of arrays, each one consisting of `num` items. (The final nested array may have fewer than `num` items.)
   * @function chunk
   * @param {array} arr Array to be chunked. This array itself will not be modified.
   * @param {number} num Number of elements per chunk
   * @returns {array} A new, chunked, array
   */
  var chunk = function(arr, n) {
    var num = parseInt(n, 10);
    var chunkedArray = [];
    var original = [].concat( arr );

    if (num < 1) {
      return arr;
    }
    while (original.length) {
      chunkedArray.push(original.splice(0, num));
    }

    return chunkedArray;
  };

  /**
   * Pad an array with `value` until its length equals `size`
   * @function pad
   * @param {array} arr Array to pad
   * @param {number} size Total length of the array after padding it
   * @param {any} value Value to use for each "padded" element of the array
   * @returns {array} The array passed to `arr`, padded
   */
  var pad = function(arr, size, value) {

    for (var i = arr.length; i < size; i++) {
      arr.push(value);
    }

    return arr;
  };

  var arrays = /*#__PURE__*/Object.freeze({
    __proto__: null,
    isArray: isArray,
    inArray: inArray,
    randomItem: randomItem,
    pluck: pluck,
    shuffle: shuffle,
    collapse: collapse,
    merge: merge,
    intersect: intersect,
    unique: unique,
    diff: diff,
    chunk: chunk,
    pad: pad
  });

  /**
   * @module object
   * @summary ES6 Import Example:
   * ```js
   * import {deepCopy} from 'fmjs';
   *
   * // or:
   * import {deepCopy} from 'fmjs/object.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {deepCopy} = require('fmjs/cjs/object.js');
   * ```
   *
   */

  var ObjectProto = Object.prototype;
  var fnProtoToString = Function.prototype.toString;

  /**
   * Indicate if the provided argument is an object/array
   * @function isObject
   * @param {Object} obj The argument that will be checked to see if it is an object
   */
  var isObject = function isObject(obj) {
    var isWindow = typeof window !== 'undefined' && obj === window;

    return typeof obj === 'object' && obj !== null && !obj.nodeType && !isWindow;
  };

  /**
  * Indicate if the provided argument is a plain object
  * Derived from lodash _.isPlainObject
  * @function isPlainObject
  * @param {Object} obj The argument that will be checked to see if it is a plain object
  */
  var isPlainObject = function(obj) {
    // If it doesn't immediately look like an object, it isn't one
    if (obj == null || typeof obj !== 'object' || ObjectProto.toString.call(obj) !== '[object Object]') {
      return false;
    }

    // Safe way to get obj's prototype, casting it to an object first, so hasOwnProperty doesn't throw error
    var objProto = Object.getPrototypeOf(Object(obj));

    // For object created with Object.create(null)
    if (objProto === null) {
      return true;
    }

    // Get the constructor of obj's prototype
    var Ctor = ObjectProto.hasOwnProperty.call(objProto, 'constructor') && objProto.constructor;

    return typeof Ctor == 'function' &&
      Ctor instanceof Ctor &&
      fnProtoToString.call(Ctor) === fnProtoToString.call(Object);
  };

  /**
  * Deep copy an object, avoiding circular references and the infinite loops they might cause.
  * @function deepCopy
  * @param {Object} obj The object to copy
  * @param {Array<Object>} [cache] Used internally to avoid circular references
  * @returns {Object} A copy of the object
  */
  var deepCopy = function deepCopy(obj, cache) {
    if ( cache === void 0 ) cache = [];

    // just return if obj is immutable value
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }

    // if obj is hit, it is in circular structure
    var hit = cache.find(function (c) { return c.original === obj; });

    if (hit) {
      return hit.copy;
    }

    var copy = isArray(obj) ? [] : {};

    // put the copy into cache at first
    // because we want to refer it in recursive deepCopy
    cache.push({
      original: obj,
      copy: copy,
    });

    Object.keys(obj).forEach(function (key) {
      copy[key] = deepCopy(obj[key], cache);
    });

    return copy;
  };

  /**
   * Deep merge two or more objects in turn, with right overriding left
   *
   * Heavily influenced by/mostly ripped off from jQuery.extend
   * @function extend
   * @param  {Object} target The target object that will be mutated. Use `{}` to create new object
   * @param  {...Object} object One or more objects to merge into the first
   * @returns {Object} The merged object
   * @example
   * const foo = {
   *   one: 'singular',
   *   two: 'are better'
   * };
   *
   * const bar = {
   *   one: 'taste',
   *   choco: 'hershey',
   *   saloon: 'wild west',
   * };
   *
   * const merged = extend(foo, bar);
   *
   * // merged is now:
   * // {
   * //  one: 'taste',
   * //  two: 'are better',
   * //  choco: 'hershey',
   * //  saloon: 'wild west',
   * // }
   *
   *
   * // because foo was mutated, it is also:
   * // {
   * //  one: 'taste',
   * //  two: 'are better',
   * //  choco: 'hershey',
   * //  saloon: 'wild west',
   * // }
   */
  var extend = function extend(tgt) {
    var rest = [], len = arguments.length - 1;
    while ( len-- > 0 ) rest[ len ] = arguments[ len + 1 ];

    var target = Object(tgt);
    var arg, prop, targetProp, copyProp;
    var hasOwn = Object.prototype.hasOwnProperty;

    if (!rest.length) {
      return deepCopy(target);
    }

    for (var i = 0; i < rest.length; i++) {
      arg = rest[i];

      if (isObject(arg)) {
        for (prop in arg) {
          targetProp = target[prop];
          copyProp = arg[prop];

          if (targetProp === copyProp) {
            continue;
          }

          if (isObject(copyProp) && hasOwn.call(arg, prop)) {
            if (isArray(copyProp)) {
              targetProp = isArray(targetProp) ? targetProp : [];
            } else {
              targetProp = isObject(targetProp) ? targetProp : {};
            }

            target[prop] = extend(targetProp, copyProp);
          } else if (typeof copyProp !== 'undefined') {
            target[prop] = copyProp;
          }
        }
      }
    }

    return target;
  };

  var getCtx = function () {
    if (typeof window !== 'undefined') {
      return window;
    }

    return typeof global !== 'undefined' ? global : {};
  };

  var ensureArray = function (properties) {
    return typeof properties === 'string' ? properties.split(/\./) : properties || [];
  };

  /**
   * Get a nested property of an object in a safe way
   * @function getProperty
   * @param  {Object} root The root object
   * @param {Array.<String>|String} properties Either an array of properties or a dot-delimited string of properties
   * @param {Any} fallbackVaue A value to assign if it's otherwise undefined
   * @returns {*} The value of the nested property, or `undefined`, or the designated fallback value
   * @example
   * const foo = {
   *   could: {
   *    keep: {
   *     going: 'but will stop'
   *   }
   * };
   *
   * console.log(getProperty(foo, 'could.keep.going'))
   * // Logs: 'but will stop'
   *
   * console.log(getProperty(foo, ['could', 'keep', 'going']))
   * // Logs: 'but will stop'
   *
   * console.log(getProperty(foo, ['broken', 'not', 'happening']))
   * // Logs: undefined
  };
   */
  var getProperty = (function(ctx) {

    return function(obj, properties, defaultVal) {
      if ( defaultVal === void 0 ) defaultVal = null;

      var root = obj || ctx;
      var props = ensureArray(properties);

      return props.reduce(function (acc, val) {
        return acc && typeof acc[val] !== 'undefined' ? acc[val] : defaultVal;
      }, root);
    };
  })(getCtx());

  /**
   * Determine whether an object (or array) is "empty"
   * @function isEmptyObject
   * @param  {Object|array} object The object to test
   * @returns {boolean} `true` if object has no keys or array no elements
   */
  var isEmptyObject = function (obj) {
    if (typeof obj !== 'object' || obj == null) {
      throw new TypeError(("Argument " + obj + " is not an object"));
    }

    return isArray(obj) ? !obj.length : !Object.keys(obj).length;
  };

  /**
   * Set a nested property of an object in a safe way
   * @function setProperty
   * @param  {Object} root The root object
   * @param {array.<String>|String} properties Either an array of properties or a dot-delimited string of properties
   * @returns {Object} The modified root object
   */
  var setProperty = (function(ctx) {

    return function(obj, properties, value) {
      var root = obj || ctx;
      var props = ensureArray(properties);

      return props.reduce(function (acc, val, i) {
        if (i === props.length - 1) {
          acc[val] = value;

          return root;
        }

        if (!acc[val]) {
          acc[val] = {};
        }

        return acc[val];
      }, root);

    };
  })(getCtx());

  /**
   * Loop through an object, calling a function for each element (like forEach, but for an object)
   * @function forEachValue
   * @param  {Object}   obj The object to iterate over
   * @param  {function} fn  A function to be called for each member of the object.
   * The function takes two parameters: the member's value and the member's key, respectively
   * @returns {undefined}
   */
  var forEachValue = function(obj, fn) {
    Object.keys(obj).forEach(function (key) { return fn(obj[key], key); });
  };


  /**
   * Return a new object containing only the properties included in the props array.
   * @function pick
   * @param {Object} obj The object from which to get properties
   * @param {array} props Propertes to get from the object
   * @returns {Object} A copy of the object, containing only the `props` properties
   */

  var pick = function(obj, props) {
    if ( props === void 0 ) props = [];

    var copy = deepCopy(obj);

    return props.reduce(function (prev, prop) {
      prev[prop] = copy[prop];

      return prev;
    }, {});
  };

  /**
   * Return a new object, excluding the properties in the props array.
   * @function omit
   * @param {Object} obj The object from which to get properties
   * @param {array} props Propertes to exclude from the object
   * @returns {Object} A modified copy of the object
   */

  var omit = function(obj, props) {
    if ( props === void 0 ) props = [];

    var copy = deepCopy(obj);

    return Object.keys(copy).reduce(function (prev, prop) {
      if (!props.includes(prop)) {
        prev[prop] = copy[prop];
      }

      return prev;
    }, {});
  };

  var objects = /*#__PURE__*/Object.freeze({
    __proto__: null,
    isObject: isObject,
    isPlainObject: isPlainObject,
    deepCopy: deepCopy,
    extend: extend,
    getProperty: getProperty,
    isEmptyObject: isEmptyObject,
    setProperty: setProperty,
    forEachValue: forEachValue,
    pick: pick,
    omit: omit
  });

  // Core FM constructor. Load this first

  var core = {
    extend: extend,

    // Get value of a nested property, without worrying about reference error
    getProperty: getProperty,

    // Number of pixels difference between touchstart and touchend necessary
    // for a swipe gesture from fm.touchevents.js to be registered
    touchThreshold: 50,
  };

  var touchThreshold = core.touchThreshold;

  // Add FM with its core methods to global window object if window is available
  if (typeof window !== 'undefined') {

    if (typeof window.FM === 'undefined') {
      window.FM = {};
    }

    for (var name in core) {
      var prop = core[name];

      if (typeof window.FM[name] === 'undefined') {
        window.FM[name] = prop;
      }
    }
  }

  var core$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    extend: extend,
    getProperty: getProperty,
    touchThreshold: touchThreshold
  });

  // Insert a <script> element asynchronously
  var addScript = function addScript(url, sid, callback) {
    var loadScript = document.createElement('script');
    var script0 = document.getElementsByTagName('script')[0];
    var done = false;

    loadScript.async = 'async';
    loadScript.src = url;

    // In case someone puts the callback in the 2nd arg.
    if (typeof sid === 'function') {
      callback = sid;
      sid = null;
    }
    sid = sid || ("s-" + (new Date().getTime()));

    // If there's a callback, set handler to call it after the script loads
    // NOTE: script may not be parsed by the time callback is called.
    if (callback) {
      loadScript.onload = loadScript.onreadystatechange = function() {
        if (!done &&
            (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')
        ) {
          done = true;
          callback();
          loadScript.onload = loadScript.onreadystatechange = null;
          script0.parentNode.removeChild(loadScript);
        }

      };
    }

    if (!document.getElementById(sid)) {
      script0.parentNode.insertBefore(loadScript, script0);
    }
  };

  // Insert a CSS <link> element in the head.
  var addLink = function addLink(params) {
    var h = document.getElementsByTagName('head')[0];
    var opts = extend({
      media: 'screen',
      rel: 'stylesheet',
      type: 'text/css',
      href: '',
    }, params);

    // bail out if the <link> element is already there
    for (var i = 0, lnks = h.getElementsByTagName('link'), ll = lnks.length; i < ll; i++) {
      if (!opts.href || lnks[i].href.indexOf(opts.href) !== -1) {
        return;
      }
    }
    var lnk = document.createElement('link');

    for (var prop in opts) {
      lnk[prop] = opts[prop];
    }
    h.appendChild(lnk);

    lnk = null;
  };

  var addElements = /*#__PURE__*/Object.freeze({
    __proto__: null,
    addScript: addScript,
    addLink: addLink
  });

  /**
   * @module url
   * @summary ES6 Import Example:
   * ```js
   * import {serialize} from 'fmjs';
   *
   * // or:
   * import {serialize} from 'fmjs/url.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {serialize} = require('fmjs/cjs/url.js');
   * ```
   *
   */

  var getObjectType = function getObjectType(obj) {
    var type = Object.prototype.toString.call(obj);

    if (type === '[object Array]') {
      return 'array';
    } else if (type === '[object Object]') {
      return 'object';
    }

    return null;
  };

  var buildParams = function buildParams(prefix, obj, options, add) {
    var val, valType;
    var objType = getObjectType(obj);
    var l = obj && obj.length;

    if (objType === 'array') {
      for (var i = 0; i < l; i++) {
        // Serialize array item.
        val = obj[i];
        valType = getObjectType(val);
        buildParams((prefix + "[" + (valType || options.indexed ? i : '') + "]"), val, options, add);
      }

    } else if (objType === 'object') {
      // Serialize object item.
      for (var name in obj) {
        buildParams((prefix + "[" + name + "]"), obj[ name ], options, add);
      }

    } else {
      // Serialize scalar item.
      if (typeof obj === 'undefined') {
        obj = '';
      }
      add(prefix, obj);
    }
  };

  var r20 = /%20/g;
  var rPlus = /\+/g;

  /**
   * Return a normalized `pathname` (old IE doesn't include initial "/" for `this.pathname`) of a passed object if it has an `href` property, or return the derived path name from string representing a URL
   * @function pathname
   * @param {Object|string} [obj = window.location]  An object with a `pathname` propety or a string representing a URL
   * @returns {string} pathname
   */

  var pathname = function(obj) {
    var el = obj || window.location;
    var pathParts = [];
    var path = el.pathname || '';

    if (typeof el === 'string') {
      // convert any URL-y string to a pathname
      if (el.indexOf('//') === 0) {
        el = location.protocol + el;
      }
      // remove "#..." and "?..."
      path = el.replace(/#.*$/, '').replace(/\?.*$/, '');
      // remove protocol, domain, etc.
      if (/^https?:\/\//.test(path)) {
        pathParts = path.split(/\//).slice(3);
        path = pathParts.join('/');
      }
    }

    path = "/" + (path.replace(/^\//, ''));

    return path;
  };

  /**
   * Return the basename of an object with `pathname` property or a string. Similar to node.js `path.basename()`
   * @function basename
   * @param {Object|string} [obj = window.location] An object with a `pathname` property, or a string representing a URL
   * @param {string} [ext] Extension (e.g. '.html') to remove from the end of the basename)
   * @returns {string} basename
   */
  var basename = function(obj, ext) {
    var rExt;
    var path = pathname(obj).split(/\//).pop() || '';

    if (ext) {
      ext = ext.replace(/\./g, '\\.');
      rExt = new RegExp((ext + "$"));
      path = path.replace(rExt, '');
    }

    return path;
  };

  /**
   * Return an array consisting of each segment of a URL path
   * @function segments
   * @param {Object|string} [obj = window.location] An object with a `pathname` property, or a string representing a URL
   * @returns {array} Array of segments
   */
  var segments = function segments(obj) {
    var path = pathname(obj).replace(/^\/|\/$/g, '');

    return path.split('/') || [];
  };

  /**
   * Return the `index`th segment of a URL path
   * @function segment
   * @param {number} index Index of the segment to return. If < 0, works like `[].slice(-n)`
   * @param {Object|string} [obj = window.location] An object with a `pathname` property, or a string representing a URL
   * @returns {array} A segment of the path derived from `obj` at `index`
   */
  var segment = function segment(rawIndex, path) {
    var index = parseInt(rawIndex, 10);
    var segs = segments(path);
    var seg = segs[index] || '';

    if (index < 0) {
      index *= -1;

      // Avoid ridiculously large number for index grinding things to a halt
      index = Math.min(index, segs.length + 1);

      while (index-- > 0) {
        seg = segs.pop() || '';
      }
    }

    return seg;
  };

  var loc = function loc(el) {
    var locat = {
      pathname: pathname(el),
      basename: basename(el),
    };
    var href, segment, host, protocol;
    var hrefParts = ['host', 'pathname', 'search', 'hash'];
    var parts = {hash: '#', search: '?'};

    if (typeof el === 'string') {

      for (var part in parts) {
        segment = el.split(parts[part]);
        locat[part] = '';

        if (segment.length === 2 && segment[1].length) {
          locat[part] = parts[part] + segment[1];
          el = segment[0];
        }
      }

      protocol = el.split(/\/\//);
      locat.protocol = protocol.length === 2 ? protocol[0] : '';
      el = protocol.pop();
      locat.host = el === locat.pathname ? location && location.host || '' : el.split('/')[0];
      host = locat.host.split(':');
      locat.hostname = host[0];
      locat.port = host.length > 1 ? host[1] : '';

      href = (locat.protocol || 'http:') + "//";

      for (var i = 0; i < hrefParts.length; i++) {
        href += locat[ hrefParts[i] ];
      }
      locat.href = href;
    } else {
      el = el || {};

      for (var key in el) {
        if (typeof locat[key] === 'undefined') {
          locat[key] = el[key];
        }
      }
    }

    return locat;
  };

  // Remove potentially harmful characters from hash and escape dots
  var hashSanitize = function hashSanitize(hash) {
    hash = hash || '';

    return hash.replace(/[^#_\-\w\d.!/]/g, '').replace(/\./g, '\\.');
  };

  //
  // options: raw, prefix, indexed
  /**
   * Convert an object to a serialized string
   * @function serialize
   * @param {Object} data Plain object to be serialized
   * @param {Object} [options] Optional settings
   * @param {boolean} [options.raw] If `true`, does NOT property values are NOT url-decoded
   * @param {string} [options.prefix] If set, and `data` is an array, sets as if prefix were the name of the array
   * @param {indexed} [options.indexed] If `true`, arrays take the form of `foo[0]=won&foo[1]=too`; otherwise, `foo[]=won&foo[]=too`
   * @returns {string} A query string
   * @example
   * console.log(serialize({foo: 'yes', bar: 'again}));
   * // Logs: 'foo=yes&bar=again'
   * @example
   * console.log(serialize({foo: ['yes', 'again']}));
   * // Logs: 'foo[]=yes&foo[]=again'
   *
   * console.log(serialize({foo: ['yes', 'again']}, {indexed: true}));
   * // Logs: 'foo[0]=yes&foo[1]=again'
   *
   * console.log(serialize(['yes', 'again'], {prefix: 'foo'}));
   * // Logs: 'foo[0]=yes&foo[1]=again'
   *
   * console.log(serialize(['yes', 'again'], {prefix: 'foo', indexed: false}));
   * // Logs: 'foo[]=yes&foo[]=again'
   */
  var serialize = function serialize(data, options) {
    options = options || {};
    var obj = {};
    var serial = [];
    var add = function(key, value) {
      var item = options.raw ? value : encodeURIComponent(value);

      serial[ serial.length ] = key + "=" + item;
    };

    if (options.prefix) {
      obj[options.prefix] = data;
    } else {
      obj = data;
    }

    // If options.prefix is set, assume we want arrays to have indexed notation (foo[0]=won)
    // Unless options.indexed is explicitly set to false
    options.indexed = options.indexed || (options.prefix && options.indexed !== false);

    if (getObjectType(obj)) {
      for (var prefix in obj) {
        buildParams(prefix, obj[prefix], options, add);
      }
    }

    return serial.join('&').replace(r20, '+');
  };

  var getParamObject = function getParamObject(param, opts) {
    var paramParts = param.split('=');
    var key = opts.raw ? paramParts[0] : decodeURIComponent(paramParts[0]);
    var val;

    if (paramParts.length === 2) {
      // First replace all '+' characters with ' '; then decode it
      val = opts.raw ? paramParts[1] : decodeURIComponent(paramParts[1].replace(rPlus, ' '));
    } else {
      val = opts.empty;
    }

    return {key: key, val: val};
  };

  /**
   * Convert a serialized string to an object
   * @function unserialize
   * @param {string} [string = location.search] Query string
   * @param {Object} [options] Optional options
   * @param {boolean} [options.raw = false] If `true`, param values will NOT be url-decoded
   * @param {Any} [options.empty = true] The returned value of a param with no value (e.g. `?foo&bar&baz`). Typically, this would be either `true` or `''`
   * @param {boolean} [options.shallow = false] If `true`, does NOT attempt to build nested object
   * @returns {Object} An object of key/value pairs representing the query string parameters
   */
  var unserialize = function unserialize(string, options) {

    if (typeof string === 'object') {
      options = string;
      string = location && location.search || '';
    } else {
      string = string || location && location.search || '';
    }

    var opts = extend({
      // if true, param values will NOT be urldecoded
      raw: false,
      // the value of param with no value (e.g. ?foo&bar&baz )
      // typically, this would be either true or ''
      empty: true,
      // if true, does not attempt to build nested object
      shallow: false,
    }, options || {});

    string = string.replace(/^\?/, '');

    var keyParts, keyRoot, keyEnd;
    var obj = {};
    // var hasBrackets = /^(.+)(\[([^\]]*)\])+$/;
    var rIsArray = /\[\]$/;

    if (!string) {
      return obj;
    }

    var params = string.split(/&/);

    for (var i = 0, l = params.length; i < l; i++) {
      var ref = getParamObject(params[i], opts);
      var key = ref.key;
      var val$1 = ref.val;

      // Set shallow key/val pair
      if (opts.shallow) {
        if (rIsArray.test(key)) {
          key = key.replace(rIsArray, '');
          obj[key] = obj[key] || [];
          obj[key].push(val$1);
        } else {
          obj[key] = val$1;
        }

        continue;
      }


      // TODO: Make the rest of this function more DRY
      // Split on brackets
      keyParts = key.replace(/\]/g, '').split('[');

      // The first element of the array is always the "root" so shift it off
      keyRoot = keyParts.shift();

      // If nothing left after root, it's just a string
      if (!keyParts.length) {
        obj[keyRoot] = val$1;
        continue;
      }

      // Now, if we still have parts of the key, we're dealing with an array or object
      keyEnd = keyParts.pop();

      // single-level array/obj
      if (!keyParts.length) {
        // array
        if (keyEnd === '') {
          obj[keyRoot] = obj[keyRoot] ? obj[keyRoot].concat(val$1) : [val$1];
        } else {
          obj[keyRoot] = obj[keyRoot] || {};
          obj[keyRoot][keyEnd] = val$1;
        }
        continue;
      }

      // nested obj -> array
      obj[keyRoot] = obj[keyRoot] || {};
      obj[keyRoot][ keyParts[0] ] = obj[keyRoot][ keyParts[0] ] ? obj[keyRoot][ keyParts[0] ].concat(val$1) : [val$1];
    }

    return obj;
  };

  var urls = /*#__PURE__*/Object.freeze({
    __proto__: null,
    pathname: pathname,
    basename: basename,
    segments: segments,
    segment: segment,
    loc: loc,
    hashSanitize: hashSanitize,
    serialize: serialize,
    unserialize: unserialize
  });

  /**
   * @module ajax
   * @summary
   * ES6 Import Example:
   * ```js
   * import {getJSON} from 'fmjs';
   *
   * // or:
   * import {getJSON} from 'fmjs/ajax.js';
   * ```
   *
   */

  var caches = {};

  var appendQs = function (url, rawData) {
    var urlParts = url.split('?');
    var glue = urlParts.length === 2 ? '&' : '?';

    if (!rawData) {
      return url;
    }

    var data = typeof rawData === 'string' ? rawData : serialize(rawData);

    return data ? ("" + url + glue + data) : url;
  };

  var setHeaders = function (xhr, headers) {
    if ( headers === void 0 ) headers = {};

    for (var h in headers) {
      xhr.setRequestHeader(h, headers[h]);
    }
  };

  var setNoCache = function (xhr, options) {
    var cache = options.cache;
    var headers = options.headers;
    var addHeaders = {};
    var testHeaders = {
      Pragma: 'no-cache',
      'Cache-Control': 'no-cache',
      'If-Modified-Since': 'Sat, 1 Jan 2000 00:00:00 GMT',
    };

    if (cache !== false) {
      return;
    }

    for (var h in testHeaders) {
      if (!headers[h]) {
        addHeaders[h] = testHeaders[h];
      }
    }

    setHeaders(xhr, addHeaders);
  };

  var setContentType = function (xhr, dataType) {
    var types = {
      json: {'Content-Type': 'application/json'},
      form: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
      formData: {'Content-Type': 'multipart/form-data'},
    };

    if (!dataType || !types[dataType]) {
      return;
    }

    setHeaders(xhr, types[dataType]);
  };

  var getResponseHeaders = function getResponseHeaders(xhr) {
    var allHeaders = xhr.getAllResponseHeaders().split('\r\n');
    var headers = {};

    allHeaders.forEach(function (item) {
      // '\u003a\u0020' is ': '
      var headerParts = item.split('\u003a\u0020');

      if (headerParts < 2) {
        return;
      }

      var key = headerParts.shift();

      headers[key] = headerParts.join(' ');
    });

    return headers;
  };

  var processResponse = function (xhr, opts) {
    if (opts.dataType === 'json' && typeof xhr.response === 'string') {
      xhr.response = JSON.parse(xhr.response);
    }

    var response = {
      xhr: xhr,
      headers: getResponseHeaders(xhr),
      timestamp: +new Date(),
    };

    var responseKeys = [
      'response',
      'responseText',
      'responseType',
      'responseURL',
      'responseXML',
      'status',
      'statusText',
      'timeout' ];

    responseKeys.forEach(function (item) {
      try {
        response[item] = xhr[item];
      } catch (e) {
        response[item] = null;
      }

    });

    return response;
  };

  var removeListeners = function (xhr, events, handlers) {
    events.forEach(function (event) {
      xhr.removeEventListener(event, handlers[event]);
    });
  };

  /**
  * Low-level ajax request
  * @function ajax
  * @param {string} [url = location.href] The URL of the resource
  * @param {{dataType: string, data: Object|string, method: string, cache: boolean, memcache: boolean, headers: Object}} [options]
  * @param {string}  [options.dataType] One of 'json', 'html', 'xml', 'form', 'formData'. Used for setting the `Content-Type` request header (e.g. `multipart/form-data` when 'formData`) and processing the response (e.g. calling JSON.parse() on a string response when 'json');
  * @param {Object|string} [options.data] Data to send along with the request. If it's a GET request and `options.data` is an object, the object is converted to a query string and appended to the URL.
  * @param {string} [options.method = GET] One of 'GET', 'POST', etc.
  * @param {boolean} [options.cache=true] If set to `false`, will not let server use cached response
  * @param {boolean} [options.memcache=false] If set to `true`, and a previous request sent to the same url was successful, will circumvent request and use the previous response
  * @param {Object} [options.headers = {}] **Advanced**: Additional headers to send with the request. If headers such as 'Accept', 'Content-Type', 'Cache-Control', 'X-Requested-With', etc.,  are set here, they will override their respective headers set automatically based on other options such as `options.dataType` and `options.cache`.
  * @returns {Promise} A resolved or rejected Promise from the server
  */
  var ajax = function(url, options) {
    if ( url === void 0 ) url = location.href;
    if ( options === void 0 ) options = {};

    var opts = Object.assign({
      method: 'GET',
      dataType: '',
      headers: {},
    }, options);
    var events = ['load', 'error', 'abort'];
    var isGet = /GET/i.test(opts.method);

    url = isGet ? appendQs(url, opts.data) : url;

    return new Promise(function (resolve, reject) {
      var sendData;
      var xhr = new XMLHttpRequest();

      var handlers = {
        error: function(err) {
          removeListeners(xhr, events, handlers);
          reject(err);
        },
        abort: function() {
          removeListeners(xhr, events, handlers);
          reject(new Error({cancelled: true, reason: 'aborted', xhr: xhr}));
        },
        load: function() {
          removeListeners(xhr, events, handlers);

          if (xhr.status >= 400 || xhr.status < 200) {
            return reject(xhr);
          }

          var processedResponse = processResponse(xhr, opts);

          if (opts.memcache) {
            caches[url] = processedResponse;
          }

          return resolve(processedResponse);
        },
      };

      if (opts.memcache && caches[url]) {
        return resolve(caches[url]);
      }

      events.forEach(function (event) {
        xhr.addEventListener(event, handlers[event]);
      });

      xhr.responseType = opts.dataType;
      xhr.open(opts.method.toUpperCase(), url);

      // Must set headers after xhr.open();
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

      if (isGet) {
        if (opts.dataType === 'json') {
          xhr.setRequestHeader('Accept', 'application/json');
        }
      } else {
        var dataType = opts.form ? '' : opts.dataType || 'form';

        sendData = opts.data;
        setContentType(xhr, dataType);
      }

      setHeaders(xhr, opts.headers);
      setNoCache(xhr, opts);

      xhr.send(sendData);
    });
  };

  /**
   * Send a GET request and return parsed JSON response from the resolved Promise
   * @function getJSON
   * @param {string} [url=location.href] The URL of the resource
   * @param {{data: Object|string, cache: boolean, memcache: boolean, headers: Object}} [options = {}] See [ajax]{@link #module_ajax..ajax} for details
   * @see [ajax]{@link #module_ajax..ajax}
   * @returns {Promise} A resolved or rejected Promise from the server
   */
  var getJSON = function (url, options) {
    if ( options === void 0 ) options = {};

    var opts = Object.assign({}, options, {
      dataType: 'json',
      method: 'GET',
    });

    return ajax(url, opts);
  };

  /**
   * Send a POST request and return parsed JSON response from the resolved Promise
   * @function postJSON
   * @param {string} [url=location.href] The URL of the resource
   * @param {{data: Object|string, cache: boolean, memcache: boolean, headers: Object}} [options = {}] See [ajax]{@link #module_ajax..ajax} for details
   * @see [ajax]{@link #module_ajax..ajax}
   * @returns {Promise} A resolved or rejected Promise from the server
   */
  var postJSON = function (url, options) {
    if ( options === void 0 ) options = {};

    var opts = Object.assign({
      dataType: 'json',
      method: 'POST',
    }, options);

    if (typeof opts.data === 'object') {
      opts.data = JSON.stringify(opts.data);
    }

    return ajax(url, opts);
  };

  /**
   * Send a POST request with `FormData` derived from form element provided by `options.form`
   * @function postFormData
   * @param {string} [url=location.href] The URL of the resource
   * @param {{form: Element, cache: boolean, memcache: boolean, headers: Object}} [options = {}] See [ajax]{@link #module_ajax..ajax} for details
   * @see [ajax]{@link #module_ajax..ajax}
   * @returns {Promise} A resolved or rejected Promise from the server
   */
  var postFormData = function (url, options) {
    if ( options === void 0 ) options = {};

    var opts = Object.assign({
      data: new FormData(options.form),
      method: 'POST',
      dataType: '',
    }, options);

    return ajax(url, opts);
  };

  var ajaxes = /*#__PURE__*/Object.freeze({
    __proto__: null,
    ajax: ajax,
    getJSON: getJSON,
    postJSON: postJSON,
    postFormData: postFormData
  });

  /* eslint-disable no-underscore-dangle */
  /**
   * @module analytics
   * @summary
   * ES6 Import Example:
   * ```js
   * import {analytics} from 'fmjs';
   *
   * // or:
   * import {analytics} from 'fmjs/analytics.js';
   * ```
   *
   */

  /**
   * Load the google analytics script and set it up to track page views. If the document title has "page not found" in it (case insensitive). It'll prepend `/404/` to the url for the page-view tracking.

   * @param {string} id The google analytics ID
   * @param {string} [type] The only possible value for the `type` argument is `'legacy'`.
   * @warning untested
   */
  var analytics = function(id, type) {
    var FM = window.FM || {};

    var url = type === 'legacy' ? 'https://ssl.google-analytics.com/ga.js' : 'https://www.google-analytics.com/analytics.js';

    var noId = !id || id.indexOf('XXXXX') !== -1;

    if (noId || document.getElementById(id)) {

      return;
    }

    var trackPageview = FM.trackPageview || ['_trackPageview'];
    var _gaq = window._gaq;

    if (type === 'legacy') {
      if (trackPageview.length === 1 && (/page not found/i).test(document.title)) {
        trackPageview.push(("/404/" + (window.location.pathname.replace(/^\//, ''))));
      }

      _gaq = [
        ['_setAccount', id],
        // add site-specific parameters here.
        // ['_setDomainName', 'none'],
        // ['_setAllowLinker', true],
        // ['_setAllowHash', false],

        // finish up
        trackPageview ];
    }

    (function(win, doc) {
      var scriptNew, script0;

      if (type === 'legacy') {
        window._gaq = _gaq;
        window.ga = function() {/* intentionally empty */};
      } else {
        win.GoogleAnalyticsObject = 'ga';
        win.ga = win.ga || function() {
          var args = [], len = arguments.length;
          while ( len-- ) args[ len ] = arguments[ len ];

          (win.ga.q = win.ga.q || []).push(args);
        };
        win.ga.l = 1 * new Date();
      }
      scriptNew = doc.createElement('script');
      script0 = doc.getElementsByTagName('script')[0];
      scriptNew.async = 1;
      scriptNew.src = url;
      scriptNew.id = id;
      script0.parentNode.insertBefore(scriptNew, script0);
    })(window, document);

    window.ga('create', id, 'auto');

    if (/page not found/i.test(document.title)) {
      window.ga('send', 'pageview', {
        page: ("/404/" + (window.location.pathname.replace(/^\//, ''))),
      });
    } else {
      window.ga('send', 'pageview');
    }

  };

  var ga = /*#__PURE__*/Object.freeze({
    __proto__: null,
    analytics: analytics
  });

  /**
   * @module event
   * @summary
   * ES6 Import Example:
   * ```js
   * import {addEvent} from 'fmjs';
   *
   * // or:
   * import {addEvent} from 'fmjs/event.js';
   * ```
   *
   */

  var listener = {
    prefix: '',
  };

  var FM = typeof window !== 'undefined' && window.FM || {};

  FM.windowLoaded = typeof document !== 'undefined' && document.readyState === 'complete';

  // See passive test: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
  var supportsObject = false;

  try {
    var options$1 = {
      // This function is called when the browser attempts to access the capture property
      get capture() {
        supportsObject = true;

        return false;
      },
    };

    window.addEventListener('test', null, options$1);
    window.removeEventListener('test', null, options$1);
  } catch (err) {
    supportsObject = false;
  }

  var hasAttachEvent = function() {
    var doc = typeof window !== 'undefined' && window.document || {};

    return doc.attachEvent === 'function' || typeof doc.attachEvent == 'object';
  };

  if (typeof addEventListener === 'function') {
    listener.type = 'addEventListener';
  } else if (hasAttachEvent()) {
    listener.type = 'attachEvent';
    listener.prefix = 'on';
  } else {
    listener.prefix = 'on';
  }

  var normalizeOptions = function (options) {
    return typeof options === 'object' ? options : {
      capture: !!options.capture,
    };
  };
  var removeEvent;

  /**
   * A wrapper around `addEventListener` that deals with browser inconsistencies (e.g. `capture`, `passive`, `once` props on `options` param; see param documentation below for details) and handles window load similar to how jQuery handles document ready by triggering  handler immediately if called *after* the event has already fired.
   * For triggering window load, this file MUST be imported before window.load occurs.
   * @function addEvent
   * @param {Element} el DOM element to which to attach the event handler
   * @param {string} type Event type
   * @param {function} handler(event) Handler function. Takes `event` as its argument
   * @param {Object|boolean} [options = false] Optional object or boolean. If boolean, indicates whether the event should be in "capture mode" rather than starting from innermost element and bubbling out. Default is `false`. If object, and browser does not support object, argument is set to capture property if provided
   * @param {boolean} [options.capture = false] Indicates if the event should be in "capture mode" rather than starting from innermost element and bubbling out. Default is `false`.
   * @param {boolean} [options.passive] If `true`, uses passive mode to reduce jank. **This is automatically set to `true`** for supported browsers if not explicitly set to `false` for the following event types: touchstart, touchmove, scroll, wheel, mousewheel. Ignored if not supported.
   * @param {boolean} [options.once] If `true`, removes listener after it is triggered once on the element.
  */
  var addEvent = function(el, type, fn, options) {
    if ( options === void 0 ) options = false;

    // Call immediately if window already loaded and calling addEvent(window, 'load', fn)
    if (FM.windowLoaded && type === 'load' && el === window) {
      return fn.call(window, {windowLoaded: true, type: 'load', target: window});
    }
    var opts = normalizeOptions(options);

    if (!supportsObject) {
      var fun = !opts.once ? fn : function(event) {
        fn(event);
        removeEvent(type, fun, opts.capture);
      };

      return el[ listener.type ](listener.prefix + type, fun, opts.capture);
    }

    var passiveEvents = ['touchstart', 'touchmove', 'scroll', 'wheel', 'mousewheel'];

    if (passiveEvents.indexOf(type) !== -1 && opts.passive !== false) {
      opts.passive = true;
    }

    return el[listener.type] && el[listener.type](listener.prefix + type, fn, opts);
  };

  // Modify addEvent for REALLY OLD browsers that have neither addEventListener nor attachEvent
  if (!listener.type) {
    addEvent = function(el, type, fn) {
      el[listener.prefix + type] = fn;
    };
  }

  /**
  * A wrapper around `removeEventListener` that naïvely deals with oldIE inconsistency.
  * @function removeEvent
  * @param {Element} el DOM element to which to attach the event handler
  * @param {string} type Event type.
  * @param {function} [handler] Handler function to remove.
  * @param {Object|boolean} [options = false] Optional object or boolean. If boolean, indicates whether event to be removed was added in "capture mode". Important: non-capturing here only removes non-capturing added event and vice-versa.
  * @param {boolean} [options.capture] Indicates whether event to be removed was added in "capture mode"
  */
  removeEvent = function(el, type, fn, options) {
    if ( options === void 0 ) options = false;

    var listenerType = typeof window.removeEventListener === 'function' ? 'removeEventListener' : document.detachEvent && 'detachEvent';
    var opts = normalizeOptions(options);

    if (listenerType) {
      return el[ listenerType ](listener.prefix + type, fn, opts.capture);
    }

    el[listener.prefix + type] = null;
  };

  if (typeof window !== 'undefined') {
    window.FM = Object.assign(window.FM || {}, FM);

    // call addEvent on window load
    if (!FM.windowLoaded) {
      addEvent(window, 'load', function () {
        FM.windowLoaded = true;
        if (window.FM) {
          window.FM.windowLoaded = true;
        }
      });
    }
  }

  var events = /*#__PURE__*/Object.freeze({
    __proto__: null,
    get addEvent () { return addEvent; },
    get removeEvent () { return removeEvent; }
  });

  /**
   * @module form
   * @summary
   * ES6 Import Example:
   * ```js
   * import {getFormData} from 'fmjs';
   *
   * // or:
   * import {getFormData} from 'fmjs/form.js';
   * ```
   *
   */

  var selectVal = function(select) {
    var val = select.type === 'select-one' ? null : [];
    var index = select.selectedIndex;
    var options = select.options || [];
    var selected = options[index];

    if (index < 0 || selected.disabled) {
      return val;
    }

    val = selected.value;

    if (val == null) {
      val = selected.text;
    }

    return val ? val.replace(/\s\s+/g, ' ').trim() : val;
  };

  var isForm = function (form) {
    return typeof form === 'object' && form.nodeName === 'FORM';
  };

  var filterSuccessfulControl = function (elem) {
    var unchecked = /radio|checkbox/.test(elem.type) && !elem.checked;

    return elem.name && !elem.disabled && !/reset|submit|button/.test(elem.type) && !unchecked;
  };


  var formElements = function (form) {
    if (!isForm(form)) {
      return [];
    }

    return [].concat( form.elements )
    .filter(filterSuccessfulControl)
    .map(function (elem) {
      return {
        name: elem.name,
        value: elem.nodeName === 'SELECT' ? selectVal(elem) : elem.value,
      };
    });
  };

  var formDataMethods = {
    string: function (form) {
      var elems = formElements(form)
      .map(function (elem) {
        return ((encodeURIComponent(elem.name)) + "=" + (encodeURIComponent(elem.value)));
      });

      return elems.join('&').replace(/%20/g, '+');
    },

    array: function (form) {
      return formElements(form);
    },

    object: function (form) {
      return unserialize(formDataMethods.string(form));
    },

    formData: function (form) {
      if (!isForm(form)) {
        return null;
      }

      var formData = new FormData();
      var elems = [].concat( form.elements );

      elems
      .filter(filterSuccessfulControl)
      .forEach(function (elem) {
        var val = elem.value;

        if (elem.nodeName === 'SELECT') {
          val = selectVal(elem);
        }

        formData.append(decodeURIComponent(elem.name), decodeURIComponent(elem.value));

        if (elem.type === 'file') {
          formData.append(elem.name, elem.files[0]);
        }
      });

      return formData;
    },
  };


  /**
   * Return the set of successful form controls of the provided `form` element in one of four types: object, string, formData, or array.
   * @namespace
   * @name getFormData
   * @param {Element} form The form element
   * @param {string} [type = object] One of 'object', 'string', 'formData', or 'array'
   * @property {function} .object(form) Return form data as an object of key/value pairs
   * @property {function} .string(form) Return form data as a query string
   * @property {function} .formData(form) Return a `FormData` instance
   * @property {function} .array(form) Return form data as an array of objects with `name` and `value` properties
   * @returns {Any} The set of successful form controls as the provided `type`
   * @example
   * const myform = document.getElementById('myform');
   *
   * console.log(getFormData.object(myform));
   * // Logs:
   * // {
   * //    email: 'name@example.com',
   * //    gender: 'female',
   * //    meals: ['breakfast', 'dinner']
   * // }
   * @example
   * const myform = document.getElementById('myform');
   *
   * console.log(getFormData.string(myform));
   * // Logs:
   * // email=name%40example.com&gender=female&meals[]=breakfast&meals[]=dinner
   * @example
   * const myform = document.getElementById('myform');
   *
   * console.log(getFormData.array(myform));
   * // Logs:
   * // [
   * //    {
   * //      name: 'email',
   * //      value: 'name@example.com'
   * //    },
   * //    {
   * //      name: 'gender',
   * //      value: 'femail'
   * //    },
   * //    {
   * //      name: 'meals[]',
   * //      value: 'breakfast'
   * //    },
   * //    {
   * //      name: 'meals[]',
   * //      value: 'dinner'
   * //    }
   * // ]
   */
  var getFormData = function (form, type) {
    if ( type === void 0 ) type = 'object';

    if (!formDataMethods[type]) {
      throw new Error(("type " + type + " not found in formDataMethods"));
    }

    return formDataMethods[type](form);
  };

  Object.keys(formDataMethods).forEach(function (key) {
    getFormData[key] = formDataMethods[key];
  });

  var forms = /*#__PURE__*/Object.freeze({
    __proto__: null,
    getFormData: getFormData
  });

  /**
   * @module jsonp
   * @summary
   * ES6 Import Example:
   * ```js
   * import {getJSONP} from 'fmjs';
   *
   * // or:
   * import {getJSONP} from 'fmjs/jsonp.js';
   * ```
   *
   */

  var getOpts = function getOpts(options, cb) {
    var opts = typeof options === 'string' ? {url: options} : options;

    opts.complete = typeof cb === 'function' ? cb : opts.complete || function() {/* intentionally empty */};
    opts.data = opts.data || {};

    return opts;
  };

  var head = document.getElementsByTagName('head')[0];

  window.jsonp = {};

  /**
   * Function for those times when you just need to make a "jsonp" request (and you can't set up CORS on the server). In other words, x-site script grabbing.
   * @function getJSONP
   * @param {Object} options
   * @param {string} options.url URL of the jsonp endpoint
   * @param {Object} [options.data] Optional data to include with the request
   * @param {string} [options.data.callback = jsonp.[timestamp]] Optional value of the callback query-string parameter to append to the script's `src`
   * @param {function} callback(json) Function to be called when request is complete. A json object is passed to it.
   * @warning untested
   * @warning requires setup on server side
   * @warning not entirely safe
   * @example
   * getJSONP({url: 'https://example.com/api/'})
   */
  var getJSONP = function(options, cb) {
    var opts = getOpts(options, cb);
    var hasJSON = typeof window.JSON !== 'undefined';
    var src = opts.url + (opts.url.indexOf('?') > -1 ? '&' : '?');

    var newScript = document.createElement('script');
    var params = [];
    var callback = "j" + (new Date().getTime());

    opts.data.callback = opts.data.callback || ("jsonp." + callback);

    // This function will be called in the other server's response
    window.jsonp[callback] = function(json) {

      if (!hasJSON) {
        json = {error: 'Your browser is too old and unsafe for this.'};
      } else if (typeof json === 'object') {
        var s = JSON.stringify(json);

        json = JSON.parse(s);
      } else if (typeof json === 'string') {
        json = JSON.parse(json);
      }

      // Callback function defined in options.complete
      // called after json is parsed.
      opts.complete(json);

      window.jsonp[callback] = null;
    };

    for (var key in opts.data) {
      params.push((key + "=" + (encodeURIComponent(opts.data[key]))));
    }
    src += params.join('&');

    newScript.src = src;

    // if (this.currentScript) head.removeChild(currentScript);
    head.appendChild(newScript);
    newScript.src = null;
  };

  var jsonp = /*#__PURE__*/Object.freeze({
    __proto__: null,
    getJSONP: getJSONP
  });

  /**
   * @module math
   * @summary ES6 Import Example:
   * ```js
   * import {median} from 'fmjs';
   *
   * // or:
   * import {median} from 'fmjs/math.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {median} = require('fmjs/cjs/math.js');
   * ```
   *
   */

  var fnType = [].reduce ? 'reduce' : 'loop';

  var fns = {
    add: function(a, b) {
      return a + b;
    },
    subtract: function(a, b) {
      return a - b;
    },
    multiply: function(a, b) {
      return a * b;
    },
    divide: function(a, b) {
      return a / b;
    },
  };

  var compute = {
    reduce: function reduce(operation) {
      return function(nums, start) {
        start = start || 0;

        return nums.reduce(fns[operation], start);
      };
    },
    loop: function loop(operation) {
      return function(nums, start) {
        start = start || 0;

        for (var i = nums.length - 1; i >= 0; i--) {
          start = fns[operation](start, nums[i]);
        }

        return start;
      };
    },
  };

  /**
   * Return the result of adding an array of numbers (sum)
   * @function
   * @param {array} array Array of numbers
   * @returns {number} Sum
   */
  var add = compute[fnType]('add');

  /**
  * Return the result of subtracting an array of numbers (difference)
  * @function
  * @param {array} array Array of numbers
  * @returns {number} Difference
  */
  var subtract = compute[fnType]('subtract');

  /**
  * Return the result of multiplying an array of numbers (product)
  * @function
  * @param {array} array Array of numbers
  * @returns {number} Product
  */
  var multiply = compute[fnType]('multiply');

  /**
  * Return the result of dividing an array of numbers (quotient)
  * @function
  * @param {array} array Array of numbers
  * @returns {number} Quotient
  */
  var divide = compute[fnType]('divide');

  /**
  * Return the remainder after dividing two numbers (modulo)
  * @function
  * @param {number|array} dividend A number representing the dividend OR an array of [dividend, divisor]
  * @param {number} [divisor] Number representing the divisor if the first argument is a number
  * @returns {number} Remainder
  */
  var mod = function mod(a, b) {
    // Allow for either two numbers or an array of two numbers
    if (arguments.length === 1 && typeof a === 'object' && a.length === 2) {
      b = a[1];
      a = a[0];
    }

    return a % b;
  };

  var numberSorter = function (a, b) {
    return a - b;
  };

  /**
   * Return the average of an array of numbers
   * @function
   * @param {array} nums Array of numbers
   * @returns {number} Average
   */
  var average = function (nums) {
    return add(nums) / nums.length;
  };

  /**
  * Return the median of an array of numbers
  * @function
  * @param {array} nums Array of numbers
  * @returns {number} Median
  */
  var median = function (nums) {
    if (nums.length < 2) {
      return nums.length === 1 ? nums[0] : 0;
    }

    var halfIndex = Math.floor(nums.length / 2);
    var sorted = [].concat( nums ).sort(numberSorter);

    // Odd numbers:
    if (nums.length % 2) {
      return sorted[halfIndex];
    }

    // Even numbers:
    var under = halfIndex - 1;

    return (sorted[under] + sorted[halfIndex]) / 2;
  };

  /**
   * Return the number with the lowest value from an array of numbers
   * @function
   * @param {array} nums Array of numbers
   * @returns {number} Minimum value
   */
  var min = function (nums) {
    var sorted = [].concat( nums ).sort(numberSorter);

    return sorted.shift();
  };

  /**
  * Return the number with the highest value from an array of numbers
  * @function
  * @param {array} nums Array of numbers
  * @returns {number} Maximum value
  */
  var max = function (nums) {
    var sorted = [].concat( nums ).sort(numberSorter);

    return sorted.pop();
  };

  var maths = /*#__PURE__*/Object.freeze({
    __proto__: null,
    add: add,
    subtract: subtract,
    multiply: multiply,
    divide: divide,
    mod: mod,
    average: average,
    median: median,
    min: min,
    max: max
  });

  /**
   * @module promise
   * @summary ES6 Import Example:
   * ```js
   * import {peach} from 'fmjs';
   *
   * // or:
   * import {peach} from 'fmjs/promise.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {peach} = require('fmjs/cjs/promise.js');
   * ```
   *
   */

  /**
   * "Promised `each()`" for iterating over an array of items, calling a function that returns a promise for each one. So, each one waits for the previous one to resolve before being called
   * @function peach
   * @param {array} arr Array to iterate over
   * @param {callback} callback(item,i) Function that is called for each element in the array, each returning a promise
   * @returns {array.<Promise>} Array of promises
   */
  var peach = function (arr, fn) {
    var funcs = arr.map(function (item, i) {
      return function () { return fn(item, i); };
    });

    return funcs.reduce(function (promise, func) {
      return promise
      .then(function (result) {
        var called = func();
        // If the function doesn't return a "then-able", create one with Promise.resolve():

        return (typeof called.then === 'function' ? called : Promise.resolve(called))
        .then([].concat.bind(result));
      });

    }, Promise.resolve([]));
  };

  var promises = /*#__PURE__*/Object.freeze({
    __proto__: null,
    peach: peach
  });

  /**
   * @module selection
   * @summary
   * ES6 Import Example:
   * ```js
   * import {getSelection} from 'fmjs';
   *
   * // or:
   * import {getSelection} from 'fmjs/selection.js';
   * ```
   *
   */

  // Use these functions for oldIE
  var normalizeCharLength = function(slicedChars, allChars) {
    var chr = '';
    var adjust = 0;

    for (var i = 0; i < slicedChars.length; i++) {
      chr = slicedChars[i];

      if (chr === '\r' && allChars[i + 1] === '\n') {
        adjust -= 1;
      }
    }

    return adjust;
  };

  var ieSetSelection = function ieSetSelection(elem, chars, startPos, endPos) {
    elem.focus();
    var textRange = elem.createTextRange();

    // Fix IE from counting the newline characters as two seperate characters
    var startChars = chars.slice(0, startPos);
    var endChars = chars.slice(startPos, endPos - startPos);
    var chr = '';
    var i = 0;

    for (i = 0; i < startChars.length; i++) {
      chr = startChars[i];

      if (chr === '\r' && chars[i + 1] === '\n') {
        startPos -= 1;
      }
    }
    startPos += normalizeCharLength(startChars, chars);
    endPos += normalizeCharLength(endChars, chars);

    textRange.moveEnd('textedit', -1);
    textRange.moveStart('character', startPos);
    textRange.moveEnd('character', endPos - startPos);
    textRange.select();
  };

  var ieGetSelection = function ieGetSelection(elem, val) {
    elem.focus();
    var range = document.selection.createRange();
    var textRange = elem.createTextRange();
    var textRangeDupe = textRange.duplicate();

    textRangeDupe.moveToBookmark(range.getBookmark());
    textRange.setEndPoint('EndToStart', textRangeDupe);

    // bail if nothing selected
    if (range == null || textRange == null) {
      return {
        start: val.length,
        end: val.length,
        length: 0,
        text: '',
      };
    }

    // For some reason IE doesn't always count the \n and \r in the length
    var textPart = range.text.replace(/[\r\n]/g, '.');
    var textWhole = val.replace(/[\r\n]/g, '.');
    var textStart = textWhole.indexOf(textPart, textRange.text.length);

    return {
      start: textStart,
      end: textStart + textPart.length,
      length: textPart.length,
      text: range.text,
    };
  };


  /**
   * Set the selection of an element's contents.
   * NOTE: If startPos and/or endPos are used on a non-input element,
   * only the first text node within the element will be used for selection
   * @function
   * @param {Element} elem The element for which to set the selection
   * @param {number} [startPos = 0] The start position of the selection. Default is 0.
   * @param {number} [endPos] The end position of the selection. Default is the last index of the element's contents.
   */
  var setSelection = function setSelection(elem, startPos, endPos) {
    startPos = startPos || 0;

    var val = elem.value || elem.textContent || elem.innerText;
    var chars = val.split('');
    var selection, range, textNode;

    if (typeof endPos === 'undefined') {
      endPos = chars.length;
    }

    // only for inputs
    if (elem.nodeName === 'INPUT' && typeof elem.selectionStart !== 'undefined') {
      elem.focus();
      elem.selectionStart = startPos;
      elem.selectionEnd = endPos;

      return this;
    }

    if (window.getSelection) {
      selection = window.getSelection();
      range = document.createRange();

      if (startPos === 0 && endPos === chars.length) {
        range.selectNodeContents(elem);
      } else {
        textNode = elem;

        while (textNode && textNode.nodeType !== 3) {
          textNode = textNode.firstChild;
        }
        range.setStart(textNode, startPos);
        range.setEnd(textNode, endPos);
      }

      if (selection.rangeCount) {
        selection.removeAllRanges();
      }

      selection.addRange(range);

      return this;
    }

    // oldIE
    if (typeof elem.createTextRange !== 'undefined') {
      ieSetSelection(elem, chars, startPos, endPos);
    }

    return this;
  };

  /**
   * Sets the selection of **all** of the element's contents (including all of its children)
   * @function
   * @param {Element} el The element for which to select all content
   * @warning untested
   */
  var setSelectionAll = function setSelectionAll(el) {
    window.getSelection().selectAllChildren(el);

    return el;
  };

  /**
   * Return an object with the following properties related to the selected text within the element:
   * * `start`: 0-based index of the start of the selection
   * * `end`: 0-based index of the end of the selection
   * * `length`: the length of the selection
   * * `text`: the selected text within the element
   * @function
   * @param {Element} el An element with selected text
   */
  var getSelection = function getSelection(el) {
    var userSelection, length;
    var elem = el || document;
    var val = elem.value || elem.textContent || elem.innerText;

    // Modern browsers

    // Inputs
    if (elem.nodeName === 'INPUT' && typeof elem.selectionStart !== 'undefined') {
      length = elem.selectionEnd - elem.selectionStart;

      return {
        start: elem.selectionStart,
        end: elem.selectionEnd,
        length: elem.selectionEnd - elem.selectionStart,
        text: elem.value.slice(elem.selectionStart, length),
      };
    }

    // Other elements
    if (window.getSelection) {
      userSelection = window.getSelection();

      return {
        start: userSelection.anchorOffset,
        end: userSelection.focusOffset,
        length: userSelection.focusOffset - userSelection.anchorOffset,
        text: userSelection.toString(),
      };
    }

    // oldIE
    if (document.selection) {
      return ieGetSelection(elem, val);
    }

    // Browser not supported
    return {
      start: val.length,
      end: val.length,
      length: 0,
      text: '',
    };

  };

  /**
   * Replace the selected text in a given element with the provided text
   * @param {Element} elem Element containing the selected text
   * @param {string} replaceString String to replace the selected text
   * @returns {Object} Selection object containing the following properties: `{start, end, length, text}`
   */
  var replaceSelection = function replaceSelection(elem, replaceString) {
    var selection = getSelection(elem);
    var startPos = selection.start;
    var endPos = startPos + replaceString.length;
    var props = ['value', 'textContent', 'innerText'];
    var prop = '';

    for (var i = 0; i < props.length; i++) {
      if (typeof elem[ props[i] ] !== 'undefined') {
        prop = props[i];
        break;
      }
    }

    elem[prop] = elem[prop].slice(0, startPos) + replaceString + elem[prop].slice(selection.end, elem[prop].length);

    setSelection(elem, startPos, endPos);

    return {
      start: startPos,
      end: endPos,
      length: replaceString.length,
      text: replaceString,
    };
  };

  var wrapSelection = function wrapSelection(elem, options) {
    var selectedText = getSelection(elem).text;
    var selection =  replaceSelection(elem, options.before + selectedText + options.after);

    if (options.offset !== undefined && options.length !== undefined) {
      selection = setSelection(elem, selection.start + options.offset, selection.start + options.offset + options.length);
    } else if (!selectedText) {
      selection = setSelection(elem, selection.start + options.before.length, selection.start + options.before.length);
    }

    return selection;
  };

  var selections = /*#__PURE__*/Object.freeze({
    __proto__: null,
    setSelection: setSelection,
    setSelectionAll: setSelectionAll,
    getSelection: getSelection,
    replaceSelection: replaceSelection,
    wrapSelection: wrapSelection
  });

  /* eslint-disable no-param-reassign */

  /**
   * @module storage
   * @summary
   * ES6 Import Example:
   * ```js
   * import {Storage} from 'fmjs';
   *
   * // or:
   * import {Storage} from 'fmjs/storage.js';
   * ```
   *
   */

  var rNormalizeNs = /([^\w$_-])+/g;
  var nsReplace = function(namespace, key) {
    var ns = (namespace || '').replace(rNormalizeNs, '');

    return key.replace(ns, '');
  };

  /**
   * Constructor for storage functions.
   * @class Storage
   * @param {string} [type = local] Type of storage: either 'local' or 'session'
   * @param {string} [namespace = fm] Namespace for keys to prevent potenial collisions with storage items used by libraries, etc.
   * @returns {this}
   */
  var Storage = function Storage(type, ns) {
    var namespace = ns;

    if (!(this instanceof Storage)) {
      return new Storage(type);
    }

    if (type === 'session') {
      this.store = window.sessionStorage;
    } else {
      this.store = window.localStorage;
    }

    namespace = (namespace || 'fm').replace(rNormalizeNs, '');
    this.namespace = namespace;
    this.length = this.getLength();

    return this;
  };

  /**
   * Get the number of items in the storage
   * @function getLength
   * @instance
   * @returns {number} The number of items
   */
  Storage.prototype.getLength = function getLength() {
    var this$1 = this;

    if (!this.namespace) {
      return this.store.length;
    }

    return Object.keys(this.store).filter(function (item) {
      return item.indexOf(this$1.namespace) === 0;
    }).length;
  };

  /**
   * Get and JSON.parse the value of the storage item identified by `key`
   * @function get
   * @instance
   * @param {string} key The key of the storage item
   * @returns {Any} The JSON.parsed value of the storage item
   */
  Storage.prototype.get = function get(key) {
    key = this.namespace + key;
    var data = this.store.getItem(key);

    return JSON.parse(data);
  };

  /**
  * Set the JSON.stringified value of the storage item identified by `key`
  * @function set
  * @instance
  * @param {string} key The key of the storage item
  * @param {Any} value The value to be set for `key`
  * @returns {string} The stringified value that is set
  */
  Storage.prototype.set = function set(key, value) {
    key = this.namespace + key;
    var data = JSON.stringify(value);

    this.store.setItem(key, data);
    this.length = this.getLength();

    return data;
  };

  /**
   * Remove the storage item identified by `key`
   * @function remove
   * @instance
   * @param {string} key The key of the storage item to remove
   */
  Storage.prototype.remove = function remove(key) {
    key = this.namespace + key;
    this.store.removeItem(key);
    this.length = this.getLength();
  };

  /**
   * Remove all storage items
   * @function clear
   * @instance
   */
  Storage.prototype.clear = function clear() {
    var this$1 = this;

    if (!this.namespace) {
      this.store.clear();
    } else {
      this.keys().forEach(function (key) {
        this$1.remove(key);
      });
    }

    this.length = 0;
  };

  /**
   * Get an object of key/value pairs of all storage items
   * @function getAll
   * @instance
   * @returns {Object} All storage items
   */
  Storage.prototype.getAll = function getAll() {
    var this$1 = this;

    var data = {};

    this.keys().forEach(function (key) {
      data[key] = JSON.parse(this$1.store.getItem(this$1.namespace + key));
    });

    return data;
  };


  /**
   * Loop through all storage items and return an array of their keys
   * @function keys
   * @instance
   * @returns {array} Array of the keys of all storage items
   */
  Storage.prototype.keys = function keys() {
    var data = [];

    for (var i = 0, len = this.store.length; i < len; i++) {
      var key = this.store.key(i);

      if (!this.namespace) {
        data.push(key);
      } else if (key.indexOf(this.namespace) === 0) {
        key = nsReplace(this.namespace, key);
        data.push(key);
      }
    }

    return data;
  };

  // Loop through all storage items, calling the callback for each one
  Storage.prototype.each = function each(callback) {
    var len = this.store.length;
    var stores = this.getAll();

    for (var key in stores) {
      var ret = callback(key, stores[key]);

      if (ret === false) {
        return;
      }
    }
  };

  Storage.prototype.map = function map(callback) {
    var stores = this.getAll();
    var data = {};

    for (var key in stores) {
      var ret = callback(key, stores[key]);

      data[key] = ret;
    }

    return data;
  };

  Storage.prototype.mapToArray = function mapToArray(callback) {
    var stores = this.getAll();
    var data = [];

    for (var key in stores) {
      var ret = callback(key, stores[key]);

      data.push(ret);
    }

    return data;
  };

  //
  Storage.prototype.filter = function filter(callback) {
    var data = {};
    var keys = this.keys();

    for (var i = 0, len = keys.length; i < len; i++) {
      var key = keys[i];
      var val = JSON.parse(this.store.getItem(this.namespace + key));

      if (callback(key, val, i)) {
        data[key] = val;
      }
    }

    return data;
  };

  // Assuming multiple storage items, with each one an object…
  // Loop through all storage items and turn them into an array of objects
  // with each object given a `key` property with value being the storage item's key
  Storage.prototype.toArray = function toArray() {
    var this$1 = this;

    return this.keys().map(function (key) {
      var val = JSON.parse(this$1.store.getItem(this$1.namespace + key));

      val.key = key;

      return val;
    });
  };

  // Loop through all storage items, like .toArray()
  // and filter them with a callback function with return value true/false
  Storage.prototype.filterToArray = function filterToArray(callback) {
    var data = [];

    this.each(function (key, val) {
      var include = callback(key, val);

      if (include) {
        val.key = key;
        data.push(val);
      }
    });

    return data;
  };

  // Merge object into a stored object (referenced by 'key')
  Storage.prototype.merge = function merge(deep, key, value) {
    var data = this.get(deep === true ? key : deep) || {};

    if (deep === true) {
      // deep extend
      data = extend(data, value);
    } else {
      // shallow, so need to rearrange args, then do a shallow copy of props
      value = key;
      key = deep;

      for (var k in value) {
        if (value.hasOwnProperty(k)) {
          data[k] = value[k];
        }
      }
    }

    this.set(key, data);

    return data;
  };

  var storages = /*#__PURE__*/Object.freeze({
    __proto__: null,
    Storage: Storage
  });

  /**
   * @module string
   * @summary ES6 Import Example:
   * ```js
   * import {slugify} from 'fmjs';
   *
   * // or:
   * import {slugify} from 'fmjs/string.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {slugify} = require('fmjs/cjs/string.js');
   * ```
   *
   */

  /**
   * @function
   * @param {string} str Word to pluralize
   * @param {number} num Number of items
   * @param {string} [ending = s] Optional ending of the pluralized word
   * @returns {string} Pluralized string
   */
  var pluralize = function pluralize(str, num, ending) {
    num = num * 1;

    if (ending === undefined) {
      ending = 's';
    }

    if (num !== 1) {
      str += ending;
    }

    return str;
  };

  var capWord = function (word) {
    var keepLower = ['and', 'or', 'a', 'an', 'the', 'her', 'his', 'its', 'our', 'your', 'their', 'about', 'above', 'across', 'after', 'against', 'along', 'among', 'around', 'at', 'before', 'behind', 'between', 'beyond', 'but', 'by', 'concerning', 'despite', 'down', 'during', 'except', 'following', 'for', 'from', 'in', 'including', 'into', 'like', 'near', 'of', 'off', 'on', 'out', 'over', 'since', 'through', 'throughout', 'to', 'towards', 'under', 'until', 'up', 'upon', 'with', 'within', 'without'];
    var rBaseWord = /^([a-zA-Z]+).*$/;
    var baseWord = word.replace(rBaseWord, '$1');

    if (keepLower.includes(baseWord)) {
      return word;
    }

    return word.slice(0, 1).toUpperCase() + word.slice(1);
  };

  var caseChanges = {
    sentence: function (str) {
      if (!str) {
        return '';
      }

      return str.slice(0, 1).toUpperCase() + str.slice(1);
    },

    title: function (str) {

      var words = (str || '').split(/\s+/);
      var firstWord = caseChanges.sentence(words.shift());

      var moreWords = words.map(capWord);

      return [firstWord ].concat( moreWords).join(' ');
    },

    caps: function (str) {
      return (str || '').toUpperCase();
    },

    camel: function (str) {
      if (!str) {
        return '';
      }

      return str
      // Lowercase the whole thing
      .toLowerCase()
      // Split on anything Not alphanumeric
      .split(/[^a-z0-9]+/)
      // Make all but first one initial cap
      .map(function (item, i) {
        return i ? item.slice(0, 1).toUpperCase() + item.slice(1) : item;
      })
      // Reassemble
      .join('');
    },

    slug: function (str) {
      return (str || '')
      .toLowerCase()
      .replace(/[^a-z0-9]+/g, '-')
      // replace repeating dashes with a single dash
      .replace(/--+/g, '-')
      // remove leading and trailing dashes
      .replace(/^-|-$/g, '') || str;
    },
  };

  /**
   * Changes the case of the provided words according to the `type`.
   * @function
   * @param {string} str String that will be cased as determined by `type`
   * @param {string} type One of 'title|sentence|caps|camel|slug'
   * @returns {string} Converted string
   * @example
   * const oldMan = 'the old man and the sea';
   *
   *console.log(changeCase(oldMan, 'title'));
   * // Logs: 'The Old Man and the Sea'
   *
   * console.log(changeCase(oldMan, 'sentence'));
   * // Logs: 'The old man and the sea'
   *
   * console.log(changeCase(oldMan, 'camel'));
   * // Logs: 'theOldManAndTheSea'
   */
  var changeCase = function (str, type) {
    if (!caseChanges[type]) {
      return str;
    }

    return caseChanges[type](str);
  };

  /**
   * Slugify a string by lowercasing it and replacing white spaces and non-alphanumerics with dashes.
   * @function
   * @param {string} str String to be converted to a slug
   * @returns {string} "Slugified" string
   * @example
   * console.log(slugify('Hello there, how are you?'));
   * // Logs: 'hello-there-how-are-you'
   *
   * console.log(slugify('  You? & Me<3* '));
   * // Logs: 'you-me-3'
   */
  var slugify = function slugify(str) {
    return caseChanges.slug(str);
  };

  /**
   * Add commas (or provided `separator`) to a number, or a string representing a number, of 1000 or greater.
   * @function
   * @param {string|number} val Number to be formatted as a string
   * @param {string} [separator = ,] punctuation to be used for thousands, millions, etc
   * @returns  {string} number formatted as string
   */
  var commafy = function commafy(val, separator) {
    if ( separator === void 0 ) separator = ',';

    val = val * 1;

    var stringVal = ("" + val).split('.');

    if (val >= 1000) {
      var int = '';
      var tmp = stringVal[0].split('');

      for (var i = tmp.length - 1, j = 0; i >= 0; i--) {
        int = tmp[i] + int;

        if (j++ % 3 === 2 && i) {
          int = "" + separator + int;
        }
      }
      stringVal[0] = int;
    }


    return stringVal.join('.');
  };

  var decimalify = function decimalify(decimal, places) {
    var i;

    var dec = decimal || '';

    if (dec.length === places) {
      return dec;
    }

    if (places == null) {
      return dec;
    }

    if (dec.length < places) {
      // If fewer decimal places than needed, "right-pad" with zeros
      for (i = dec.length; i < places; i++) {
        dec += '0';
      }
    } else if (dec.length > places) {
      // eslint-disable-next-line no-restricted-properties
      dec = Math.round((dec * 1) / Math.pow(10, dec.length - places));
      dec = "" + dec;

      // After rounding to the proper level, need to "left-pad" to match number of decimal places
      // For example, rounding to 2 decimal places, 0472323 will change to 05, which will be read only as 5
      for (i = dec.length; i < places; i++) {
        dec = "0" + dec;
      }
    }

    return dec;
  };

  // Convert a number to a formatted string
  var formatNumber = function formatNumber(val, options) {
    options = options || {};
    var numParts = [];
    var num = '';
    var output = '';
    var opts = extend({
      prefix: '',
      suffix: '',
      // decimalPlaces: undefined
      includeHtml: typeof options.prefix === 'string' && options.prefix.indexOf('$') !== -1,
      classRoot: 'Price',
    }, options);

    var cr = opts.classRoot;

    num = "" + val;
    numParts = num.split('.');

    numParts[1] = decimalify(numParts[1], opts.decimalPlaces);

    numParts[0] = commafy(numParts[0]);

    if (opts.decimalPlaces === 0) {
      numParts = numParts.slice(0, 1);
    }

    if (opts.includeHtml) {
      output = "<span class=\"" + cr + "\"><span class=\"" + cr + "-prefix\">" + (opts.prefix) + "</span>";
      output += "<span class=\"" + cr + "-int\">" + (numParts[0]) + "</span>";

      if (opts.decimalPlaces !== 0) {
        output += "<span class=\"" + cr + "-dec\">.</span>";
        output += "<span class=\"" + cr + "-decNum\">" + (numParts[1]) + "</span>";
      }

      output += opts.suffix ? ("<span class=\"" + cr + "-suffix\">" + (opts.suffix) + "</span>") : '';
      output += '</span>';
    } else {
      output = opts.prefix + numParts.join('.') + opts.suffix;
    }

    return output;
  };

  /**
   * ROT13 encode/decode a string
   * @function
   * @param {string} string String to be converted to or from ROT13
   * @returns {string} The encoded (or decoded) string
   */
  var rot13 = function rot13(string) {
    var str = string.replace(/[a-zA-Z]/g, function (c) {
      var cplus = c.charCodeAt(0) + 13;

      return String.fromCharCode((c <= 'Z' ? 90 : 122) >= cplus ? cplus : cplus - 26);
    });

    return str;
  };

  /**
   * Convert a string to Java-like numeric hash code
   * @function
   * @param {string} str String to be converted
   * @param {string} [prefix] Optional prefix to the hash
   * @returns {number|string} The converted hash code as numeral (or string, if prefix is provided)
   * @see http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
   */
  var hashCode = function hashCode(str, prefix) {
    var hash = 0;
    var chr;

    if (str.length === 0) {
      return hash;
    }

    for (var i = 0, len = str.length; i < len; i++) {
      chr = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      // Convert to 32bit integer
      hash |= 0;
    }

    hash = hash >= 0 ? hash : hash * -1;

    if (prefix) {
      hash = prefix + hash;
    }

    return hash;
  };

  /**
   * Return a base64-encoded string based on the provided string.
   * If the browser does not support this type of encoding, returns the string unchanged.
   * @function
   * @param {string} str String to be base4 encoded
   * @returns {string} base64-encoded string
   */
  var base64Encode = function base64Encode(str) {
    if (typeof btoa === 'undefined') {
      return str;
    }

    return btoa(encodeURIComponent(str));
  };

  /**
  * Return a decoded string based on the provided base64-encoded string.
  * If the browser does not support this type of encoding, returns the string unchanged.
  * @function
  * @param {string} str base4-encoded string
  * @returns {string} decoded string
  */
  var base64Decode = function base64Decode(str) {
    if (typeof atob === 'undefined') {
      return str;
    }

    return decodeURIComponent(atob(str));
  };

  var strings = /*#__PURE__*/Object.freeze({
    __proto__: null,
    pluralize: pluralize,
    changeCase: changeCase,
    slugify: slugify,
    commafy: commafy,
    decimalify: decimalify,
    formatNumber: formatNumber,
    rot13: rot13,
    hashCode: hashCode,
    base64Encode: base64Encode,
    base64Decode: base64Decode
  });

  /**
   * @module timer
   * @summary ES6 Import Example:
   * ```js
   * import {debounce} from 'fmjs';
   *
   * // or:
   * import {debounce} from 'fmjs/timer.js';
   * ```
   *
   * CommonJS Require Example:
   * ```js
   * const {debounce} = require('fmjs/cjs/timer.js');
   * ```
   *
   */

  var SECOND = 1000;
  var MINUTE = SECOND * 60;
  /**
   * Constant representing the number of milliseconds in an hour
   * @const {number} HOUR
   */
  var HOUR = MINUTE * 60;
  /**
  * Constant representing the number of milliseconds in a day
  * @const {number} DAY
  */
  var DAY = HOUR * 24;
  /**
  * Constant representing the number of milliseconds in a year
  * @const {number} YEAR
  */
  var YEAR = DAY * 365;

  /**
   * Set up a function to be called once at the end of repeated potential calls within a given delay
   * @function debounce
   * @param {function} fn The function to trigger once at the end of a series of potential calls within `delay`
   * @param {number} [timerDelay = 200] Number of milliseconds to delay before firing once at the end
   * @param {Element} [ctx = this] The context in which to call `fn`
   * @example
   * const scrollLog = function(event) {
   * console.log('Started resizing the window!');
   * };
   *
   * window.addEventListener('resize', debounce(scrollLog));
   */
  var debounce = function debounce(fn, timerDelay, ctx) {
    var delay = timerDelay === undefined ? 200 : timerDelay;
    var timeout;

    return function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      ctx = ctx || this;

      window.clearTimeout(timeout);
      timeout = window.setTimeout(function () {
        fn.apply(ctx, args);
        timeout = ctx = args = null;
      }, delay);
    };
  };

  /**
  * Set up a function to be called once at the end of repeated potential calls within a given delay
  * @function unbounce
  * @param {function} fn The function to trigger once at the beginning of a series of potential calls within `delay`
  * @param {number} [timerDelay = 200] Number of milliseconds within which to avoid calling the same function
  * @param {Element} [ctx = this] The context in which to call `fn`
  * @example
  * const scrollLog = function(event) {
  * console.log('Started resizing the window!');
  * };
  *
  * window.addEventListener('resize', debounce(scrollLog));
  */
  var unbounce = function unbounce(fn, timerDelay, ctx) {
    var delay = timerDelay === undefined ? 200 : timerDelay;
    var timeout;

    return function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      if (!timeout) {
        fn.apply(ctx || this, args);
      }

      window.clearTimeout(timeout);
      timeout = window.setTimeout(function () {
        timeout = null;
      }, delay);
    };
  };

  /**
  * Set up a function to be called no more than once every `timerDelay` milliseconds
  * @function throttle
  * @param {function} fn The function to throttle
  * @param {number} [timerDelay = 200] Number of milliseconds to throttle the function calls
  * @param {Element} [ctx = this] The context in which to call `fn`
  */
  var throttle = function throttle(fn, timerDelay, context) {
    var delay = timerDelay === undefined ? 200 : timerDelay;
    var previous = 0;
    var timedFn;

    return function() {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      var ctx = context || this;
      var now = +new Date();

      if (!previous) {
        fn.apply(ctx, args);
        previous = now;
      } else {
        clearTimeout(timedFn);
        timedFn = setTimeout(function () {
          var then = +new Date();

          if (then - previous >= delay) {
            fn.apply(ctx, args);
            previous = then;
          }
        }, delay - (now - previous));
      }
    };
  };

  var timers = /*#__PURE__*/Object.freeze({
    __proto__: null,
    SECOND: SECOND,
    MINUTE: MINUTE,
    HOUR: HOUR,
    DAY: DAY,
    YEAR: YEAR,
    debounce: debounce,
    unbounce: unbounce,
    throttle: throttle
  });

  // http://davidwalsh.name/css-animation-callback
  var detectEndName = function detectEndName(type) {
    var el = document.createElement('fake');
    var names = {
      transition: {
        transition: 'transitionend',
        WebkitTransition: 'webkitTransitionEnd',
        MozTransition: 'transitionend',
      },
      animation: {
        animation: 'animationend',
        WebkitAnimation: 'webkitAnimationEnd',
        MozAnimation: 'animationend',
      },
    };
    var testNames = names[type];

    for (var t in testNames) {
      if (el.style[t] !== undefined) {
        return testNames[t];
      }
    }
  };

  var transitionEnd = detectEndName('transition');
  var animationEnd = detectEndName('animation');

  var transEvents = /*#__PURE__*/Object.freeze({
    __proto__: null,
    transitionEnd: transitionEnd,
    animationEnd: animationEnd
  });

  // get the ID of a youtube video from an element or its href or its src
  var getYoutubeId = function getYoutubeId(link) {
    var id = '';
    var params = {};
    var href = typeof link === 'string' ? link : link.href || link.src;
    var hrefParts = href.split('?');

    if (hrefParts.length === 2) {
      params = unserialize(hrefParts[1]);
      id = params.v || params.embed;
    }

    if (!id) {
      id = hrefParts[0].split(/\//).pop();
    }

    return id || '';
  };

  var youtubes = /*#__PURE__*/Object.freeze({
    __proto__: null,
    getYoutubeId: getYoutubeId
  });

  /**
   * @module color
   * @summary
   * ES6 Import Example
   * ```js
   * import {rgb2Hex} from 'fmjs'
   *
   * // or:
   * import {rgb2Hex} from 'fmjs/color.js'
   * ```
   *
   * CJS Require Example
   * ```js
   * const {rgb2Hex} = require('fmjs/cjs/color.js');
   * ```
   */

  var getLongHex = function (hex) {
    var parts = hex.split('');

    if (parts.length > 4) {
      return parts.slice(0, 6).join('');
    }

    return parts.slice(0, 3).map(function (part) { return ("" + part + part); }).join('');
  };

  var normalizeHex = function (hex) {
    var rawColor = ("" + hex).replace(/^#/, '');
    var len = rawColor.length;
    var color = getLongHex(rawColor);


    if (len % 3 === 0) {
      return {
        color: color,
        alpha: 1,
      };
    }

    // Handle hex with alpha
    if (len % 4 === 0) {
      var parts = rawColor.split('');
      var alpha = len === 4 ? parts.slice(-1).concat( parts.slice(-1)) : parts.slice(-2);

      return {
        color: color,
        // First convert from hex number to 0-255, then get 0-1:
        alpha: parseInt(alpha.join(''), 16) / 255,
      };
    }

    throw new Error(("Broken color: " + rawColor));
  };

  var hex2RgbArray = function (hex) {
    var ref  = normalizeHex(hex);
    var color = ref.color;
    var alpha = ref.alpha;
    var colorParts = color.split('');
    var rgb = ['r', 'g', 'b'].map(function (_, i) {
      return parseInt(colorParts.slice(i * 2, i * 2 + 2).join(''), 16);
    });

    if (alpha !== 1) {
      rgb.push(alpha);
    }

    return rgb;
  };


  /**
   * Convert a hex value to an rgb or rgba value
   * @param {string} hex Hex color code in shorthand format (e.g. #333, #333a) or longhand (e.g. #333333, #333333aa)
   * @param {number} [alpha] Optional number from 0 to 1 to be used with 3- or 6-character hex format
   */
  var hex2Rgb = function (hex, alpha) {
    var rgb = hex2RgbArray(hex);

    if (typeof alpha !== 'undefined') {
      rgb.push(alpha);
    }

    var name = rgb.length === 4 ? 'rgba' : 'rgb';

    return (name + "(" + (rgb.join(', ')) + ")");
  };

  var rgbString2Array = function (str) {
    if (typeof str !== 'string') {
      return str;
    }

    var color = str.trim().replace(/^rgba?\(([^)]+)\)/, '$1');

    return color.split(/\s*,\s*/)
    .map(function (c) { return c * 1 || 0; });
  };

  /**
   * Convert an rgb value to a 6-digit hex value. If an *rgba* value is passed, the opacity is ignored
   * @function rgb2Hex
   * @param {string|array} rgb either an rgb string such as `'rgb(255, 120, 10)'` or an rgb array such as `[255, 120, 10]`
   * @returns {string} Hex value (e.g. `#ff780a`)
   * @example
   * rgb2Hex('rgb(255, 136, 0)')
   * // => '#ff8800'
   *
   * rgb2Hex([255, 136, 0])
   * // => '#ff8800'
   *
   * rgb2Hex('rgba(255, 136, 0, .8)')
   * // => '#ff8800'
   */
  var rgb2Hex = function (rgb) {
    var colorParts = typeof rgb === 'string' ? rgbString2Array(rgb) : rgb;
    var hex = colorParts.slice(0, 3).map(function (item) {
      return item < 16 ? ("0" + (item.toString(16))) : item.toString(16);
    });

    return ("#" + (hex.join('')));
  };

  /**
   * Convert an rgba value to an 8-digit hex value, or an rgb value to a 6-digit hex value
   * @function rgba2Hex
   * @param {string|array} rgba either an rgba string such as `'rgba(255, 120, 10, .5)'` or an rgba array such as `[255, 120, 10, .5]`
   * @returns {string} Hex value (e.g. `#ff780a80`)
   * @example
   * rgba2Hex('rgba(255, 136, 0, .8)')
   * // => '#ff8800cc'
   *
   * rgba2Hex([255, 136, 0, .8])
   * // => '#ff8800cc'
   *
   * rgba2Hex('rgb(255, 136, 0)')
   * // => '#ff8800'
   */
  var rgba2Hex = function (rgb) {
    var colorParts = typeof rgb === 'string' ? rgbString2Array(rgb) : rgb;
    var hex = colorParts.map(function (part, i) {
      var item = i === 3 ? Math.round(part * 255) : part;

      return item < 16 ? ("0" + (item.toString(16))) : item.toString(16);
    });

    return ("#" + (hex.join('')));
  };

  /**
   * Convert an RGB color to a luminance value. You probably don't want to use this on its own
   * @function rgb2Luminance
   * @param {string|array} rgb RGB value represented as a string (e.g. `rgb(200, 100, 78)`) or an array (e.g. `[200, 100, 78]`)
   * @see [getContrastColor()]{@link #module_color..getContrastColor}
   * @see [StackOverflow]{@link https://stackoverflow.com/questions/9733288/how-to-programmatically-calculate-the-contrast-ratio-between-two-colors} for more information
   * @returns {number} The luminance value
   */
  var rgb2Luminance = function (rgb) {
    var colors = typeof rgb === 'string' ? rgbString2Array(rgb) : rgb;
    var lumParts = colors.map(function (color) {
      var col = color / 255;

      if (col <= 0.03928) {
        return col / 12.92;
      }

      return Math.pow( ((col + 0.055) / 1.055), 2.4 );
    });

    return [0.2126, 0.7152, 0.0722].reduce(function (sum, curr, i) {
      return sum + (curr * lumParts[i]);
    }, 0);
  };

  /**
   * Return darkColor if bgColor is light and lightColor if bgColor is dark. "Light" and "dark" are determined by the [rgb2Luminance]{@link #module_color..rgb2Luminance} algorithm
   * @function getContrastColor
   * @param {string} bgColor hex code (e.g. #daf or #3d31c2) of the color to contrast
   * @param {string} [darkColor = #000] The dark color to return if `bgColor` is considered light
   * @param {string} [lightColor = #fff] The light color to return if `bgColor` is considered dark
   * @returns {string} Contrasting color
   * @warning untested
   */
  var getContrastColor = function (bgColor, darkColor, lightColor) {
    if ( darkColor === void 0 ) darkColor = '#000';
    if ( lightColor === void 0 ) lightColor = '#fff';

    var rgb = hex2RgbArray(bgColor);
    var luminance = rgb2Luminance(rgb);

    return luminance > 0.179 ? darkColor : lightColor;
  };


  var simpleContrast = function (bgColor, darkColor, lightColor) {
    if ( darkColor === void 0 ) darkColor = '#000';
    if ( lightColor === void 0 ) lightColor = '#fff';

    var rgb = hex2RgbArray(bgColor);
    var simpleLuminance = [0.299, 0.587, 0.114].reduce(function (sum, curr, i) {
      return sum + (curr * rgb[i]);
    }, 0);

    return simpleLuminance > 186 ? darkColor : lightColor;
  };

  var colors = /*#__PURE__*/Object.freeze({
    __proto__: null,
    hex2Rgb: hex2Rgb,
    rgb2Hex: rgb2Hex,
    rgba2Hex: rgba2Hex,
    rgb2Luminance: rgb2Luminance,
    getContrastColor: getContrastColor,
    simpleContrast: simpleContrast
  });

  /**
   * @module dom
   * @summary
   * ES6 Import Example:
   * ```js
   * import {addClass} from 'fmjs';
   *
   * // or:
   * import {addClass} from 'fmjs/dom.js';
   * ```
   *
   */

  function objectWithoutProperties (obj, exclude) { var target = {}; for (var k in obj) if (Object.prototype.hasOwnProperty.call(obj, k) && exclude.indexOf(k) === -1) target[k] = obj[k]; return target; }


  var isNode = function (obj) {
    return obj instanceof Node || obj && obj.nodeType >= 1;
  };

  /**
   * Converts a selector string, DOM element, or collection of DOM elements into an array of DOM elements
   * @function toNodes
   * @param {Element|NodeList|array|string} element(s) The selector string, element, or collection of elements (NodeList, HTMLCollection, Array, etc)
   * @returns {array} An array of DOM elements

  */
  var toNodes = function (element) {
    if (typeof element === 'string') {
      return [].concat( document.querySelectorAll(element) );
    }

    if (!element) {
      return [];
    }

    if (isNode(element)) {
      return [element];
    }

    return element.filter(function (item) { return isNode(item); });
  };

  /**
  * Return an array of DOM Nodes within the document or provided element/nodelist
  * @function $
  * @param {string} selector The CSS selector of the DOM elements
  * @param {Element|NodeList|array|string} [context = document] The selector string, element, or collection of elements (NodeList, HTMLCollection, Array, etc) representing one or more elements within which to search for `selector`
  * @returns {Array} Array of DOM nodes matching the selector within the context
  */
  var $ = function (selector, context) {
    if (typeof context !== 'undefined' && !context) {
      return [];
    }
    var contexts = !context ? [document] : toNodes(context);

    return contexts.reduce(function (arr, ctx) {
      arr.push.apply(arr, ctx.querySelectorAll(selector));

      return arr;
    }, []);
  };

  /**
  * Return the first found DOM Element within the document or provided element/nodelist/HTMLCollection
  * @function $1
  * @param {string} selector Selector string for finding the DOM element
  * @param {Element|NodeList|array|string} [context = document] The selector string, element, or collection of elements (NodeList, HTMLCollection, Array, etc) representing one or more elements within which to search for `selector`
  * @returns {Element} First DOM Element matching the selector within the context
  */
  var $1 = function (selector, context) {
    if (typeof context === 'undefined' || isNode(context)) {
      return (context || document).querySelector(selector);
    }

    var ctx = toNodes(context);

    // Find the first element matched by selector within each context element.
    // Then, filter the array to include only the items that matched
    // Finally, return the first one
    return ctx
    .map(function (el) { return el.querySelector(selector); })
    .filter(function (el) { return !!el; })
    [0];
  };

  var rTrim = /^\s+|\s$/g;

  // Class functions:

  /**
   * Add one or more classes to an element
   * @function addClass
   * @param  {Element} el - DOM element for which to add the class
   * @param {string} className class to add to the DOM element
   * @param {...string} [classNameN] one or more additional className arguments representing classes to add to the element
   * @returns {string} the resulting class after classes have been removed
   */
  var addClass = (function () {
    if (document.body.classList) {
      return function (el) {
        var ref;

        var classNames = [], len = arguments.length - 1;
        while ( len-- > 0 ) classNames[ len ] = arguments[ len + 1 ];
        (ref = el.classList).add.apply(ref, classNames);

        return el.className;
      };
    }

    return function (el) {
      var names = [], len = arguments.length - 1;
      while ( len-- > 0 ) names[ len ] = arguments[ len + 1 ];

      var classes = " " + (el.className) + " ";

      if (names && names.length) {
        for (var i = 0; i < names.length; i++) {
          if (classes.indexOf((" " + (names[i]) + " ")) === -1) {
            classes += (names[i]) + " ";
          }
        }

        el.className = classes.replace(rTrim, '');
      }

      return el.className;
    };
  })();

  /**
   * Remove one or more classes from an element
   * @function removeClass
   * @param  {Element} el DOM element from which to remove the class
   * @param {string} className class to remove from the DOM element
   * @param {...string} [classNameN] one or more additional className arguments representing classes to remove from the element
   * @returns {string} the resulting class after classes have been removed
   */
  var removeClass = (function () {
    if (document.body.classList) {
      return function (el) {
        var ref;

        var names = [], len = arguments.length - 1;
        while ( len-- > 0 ) names[ len ] = arguments[ len + 1 ];
        (ref = el.classList).remove.apply(ref, names);
      };
    }

    return function (el) {
      var names = [], len = arguments.length - 1;
      while ( len-- > 0 ) names[ len ] = arguments[ len + 1 ];

      var classes = " " + (el.className) + " ";

      if (names && names.length) {
        for (var i = 0; i < names.length; i++) {
          if (classes.indexOf((" " + (names[i]) + " ")) !== -1) {
            classes = classes.replace(((names[i]) + " "), '');
          }
        }
      }
      el.className = classes.replace(rTrim, '');

      return el.className;
    };
  })();

  /**
   * Add a class if it's not present (or if toggle is true); remove the class if it is present (or if toggle is false)
   * @function toggleClass
   * @param {Element} el Element on which to toggle the class
   * @param {string} className The class name to either add or remove
   * @param {boolean} [toggle] Optional boolean argument to indicate whether className is to be added (true) or removed (false)
   */
  var toggleClass = (function () {
    var div = document.createElement('div');
    // Check for *full* toggle support, including 2nd "toggle/force" argument
    var hasClassListToggle = div.classList && div.classList.toggle && div.classList.toggle('a', 0) === false;

    div = null;

    if (hasClassListToggle) {
      return function (el, className, toggle) {
        return el.classList.toggle(className, toggle);
      };
    }

    return function (el, className, toggle) {
      var classes = " " + (el.className) + " ";

      if ((typeof toggle === 'undefined' && classes.indexOf((" " + className + " ")) === -1) || toggle) {
        addClass(el, className);
      } else {
        removeClass(el, className);
      }
    };
  })();

  /**
   * Replace oldClass with newClass
   * @function replaceClass
   * @param {Element} el DOM element for which you want to replace oldClass with newClass
   * @param {string} oldClass The class name you want to get rid of
   * @param {string} newClass The class name you want to add in place of oldClass
   * @returns {string} The `className` property of the element after the class has been replaced
   */
  var replaceClass = function (el, oldClass, newClass) {
    if (el.classList) {
      return el.classList.replace(oldClass, newClass);
    }

    var classes = el.className.replace(rTrim).split(/\s+/);

    el.className = classes
    .map(function (item) {
      return item === oldClass ? newClass : item;
    })
    .filter(function (item, i, arr) {
      return arr.indexOf(item) === i;
    })
    .join(' ');

    return el.className;
  };

  /**
   * Get the top and left distance to the element (from the top of the document)
   * @function getOffset
   * @param {Element} el Element for which to get the offset
   * @warning untested
   * @returns {{top: number, left: number}} Object with `top` and `left` properties representing the top and left offset of the element
   */
  var getOffset = function (el) {
    if (!el) {
      return {};
    }
    var rect = el.getBoundingClientRect();
    var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    return {top: rect.top + scrollTop, left: rect.left + scrollLeft, scrollTop: scrollTop, scrollLeft: scrollLeft};
  };

  var booleanAttrs = [
    'async',
    'autofocus',
    'autoplay',
    'checked',
    'controls',
    'default',
    'defer',
    'disabled',
    'formnovalidate',
    'frameborder',
    'hidden',
    'indeterminate',
    'ismap',
    'loop',
    'multiple',
    'muted',
    'novalidate',
    'open',
    'readonly',
    'required',
    'reversed',
    'scoped',
    'selected' ];

  /**
  * Set one or more styles on an element.
  * @function setStyles
  * @param {Element} el element on which to add styles
  * @param {Object.<string, string|number>} styles object of styles and their values to add to the element
  * @returns {Element} The original element, with the styles set
  */
  var setStyles = function (el, styles) {
    if (!el || !styles) {
      return el;
    }
    Object.keys(styles || {}).forEach(function (key) {
      el.style[key] = styles[key];
    });

    return el;
  };

  /**
   * Set one or more attributes on an element. For boolean attributes ('async', 'required', etc.), set the element's property to either `true` or `false`
   * @function setAttrs
   * @param {Element} el element on which to add attributes
   * @param {Object.<string, string|boolean|number>} attrs object of attributes and their values to add to the element
   * @returns {Element} The original element, with the attributes set
   */
  var setAttrs = function (el, attrs) {
    if (!el || !attrs) {
      return el;
    }

    var mappedProps = {
      class: 'className',
      className: 'className',
      for: 'htmlFor',
      htmlFor: 'htmlFor',
    };

    Object.keys(attrs).forEach(function (key) {
      var val = attrs[key];

      if (mappedProps[key]) {
        el[mappedProps[key]] = val;
      } else if (booleanAttrs.indexOf(key) !== -1) {
        if (val) {
          el.setAttribute(key, 'value');
        } else {
          el.removeAttribute(key);
        }
      } else {
        el.setAttribute(key, val === true ? '' : val);
      }
    });

    return el;
  };

  /**
   * Given an array of attribute names, get an object containing attribute names/values for an element
   * @function getAttrs
   * @param {Element} el DOM Element. If NodeList is provided, uses the first element in the list
   * @param {array.<string>} attrs Array of attribute names
   * @returns {Object} Object of attribute names along with their values
   */
  var getAttrs = function (el, attrs) {
    if ( attrs === void 0 ) attrs = [];

    var element = toNodes(el)[0];

    if (!element) {
      return {};
    }

    return attrs.reduce(function (obj, key) {
      obj[key] = element.getAttribute(key);

      return obj;
    }, {});
  };


  var traversals = {
    afterbegin: 'firstChild',
    afterend: 'nextSibling',
    beforebegin: 'previousSibling',
    beforeend: 'lastChild',
  };

  var insertHTML = function (element, position, toInsert) {
    var els = toNodes(element);
    var traversalProp = traversals[position];

    return els
    .map(function (el) {
      el.insertAdjacentHTML(position, toInsert);

      return el[traversalProp];
    });
  };

  /**
   * Insert an element as the first child of `el`
   * @function prepend
   * @param {Element} el Reference element
   * @param {Element|string} toInsert DOM element or HTML string to insert as the first child of `el`
   * @returns {Element} The inserted element
   */
  var prepend = function (el, toInsert) {
    if (typeof toInsert === 'string') {
      return insertHTML(el, 'afterbegin', toInsert);
    }

    if (el.childNodes) {
      el.insertBefore(el.firstChild, toInsert);
    } else {
      el.appendChild(toInsert);
    }

    return el.firstChild;
  };

  /**
   * Insert an element as the last child of `el`
   * @function append
   * @param {Element} el Reference element
   * @param {Element|string} toInsert DOM element or HTML string to insert as the last child of `el`
   * @returns {Element} The inserted element
   */
  var append = function (el, toInsert) {
    if (typeof toInsert === 'string') {
      return insertHTML(el, 'beforeend', toInsert);
    }

    el.appendChild(toInsert);

    return el.lastChild;
  };

  /**
   * Insert an element as the previous sibling of `el`
   * @function before
   * @param {Element} el Reference element
   * @param {Element|string} toInsert DOM element or HTML string to insert as the previous sibling of `el`
   * @returns {Element} The inserted element
   */
  var before = function (el, toInsert) {
    if (typeof toInsert === 'string') {
      return insertHTML(el, 'beforebegin', toInsert);
    }

    el.parentNode.insertBefore(el, toInsert);

    return el.previousSibling;
  };

  /**
   * Insert an element as the next sibling of `el`
   * @function after
   * @param {Element} el Reference element
   * @param {Element|string} toInsert DOM element or HTML string to insert as the next sibling of `el`
   * @returns {Element} The inserted element
   */
  var after = function (el, toInsert) {
    if (typeof toInsert === 'string') {
      return insertHTML(el, 'afterend', toInsert);
    }

    if (el.nextSibling) {
      el.parentNode.insertBefore(el.nextSibling, toInsert);
    } else {
      el.parentNode.appendChild(toInsert);
    }

    return el.nextSibling;
  };

  /**
   * Provide an object, along with possible child objects, to create a node tree ready to be inserted into the DOM.
   * @function createTree
   * @param {Object} options
   * @param {string} [options.tag] Optional tag name for the element. If none provided, a document fragment is created instead
   * @param {string} [options.text] Optional inner text of the element.
   * @param {Array<Object>} [options.children] Optional array of objects, with each object representing a child node
   * @param {string} [...options[attr]] One or more optional attributes to set on the element
   * @returns {Element(s)} The created Element node tree
   */
  var createTree = function(ref) {
    var tag = ref.tag;
    var text = ref.text;
    var children = ref.children; if ( children === void 0 ) children = [];
    var rest = objectWithoutProperties( ref, ["tag", "text", "children"] );
    var attrs = rest;

    var el;

    if (tag) {
      el = document.createElement(tag || 'div');
      setAttrs(el, attrs);
    } else {
      el = document.createDocumentFragment();
    }

    if (text) {
      el.appendChild(document.createTextNode(text));
    }

    children.forEach(function (child) {
      el.appendChild(createTree(child));
    });

    return el;
  };

  // Remove as many event handlers as possible from element, as well as its children
  // Currently only works with handlers attached as attributes
  var cleanHandlers = function (el) {
    var attrs = el.attributes;

    // Clean event handlers attached as an attribute (e.g. <div onclick="function() {}")
    if (attrs && attrs.length) {
      for (var i = attrs.length - 1; i >= 0; i--) {
        var name = attrs[i].name;

        // Null out event handlers:
        if (typeof el[name] === 'function') {
          el[name] = null;
        }
      }
    }

    var children = el.childNodes;

    if (children && children.length) {
      var len = children.length;

      for (var i$1 = 0; i$1 < len; i$1++) {
        cleanHandlers(el.childNodes[i$1]);
      }
    }
  };

  /**
   * Remove an element from the DOM
   * @function remove
   * @param {Element} el DOM element to be removed
   * @returns {Element} DOM element removed from the DOM
   */
  var remove = function (el) {
    if (!el || !el.nodeName) {
      return;
    }

    cleanHandlers(el);

    if (typeof el.remove === 'function') {
      el.remove();
    } else if (el.parentNode) {
      el.parentNode.removeChild(el);
    }

    return el;
  };

  /**
   * Empty an element's children from the DOM
   * @function empty
   * @param {Element} el DOM element to clear of all children
   * @returns {Element} DOM element provided by `el` argument
   */
  var empty = function (el) {
    if (!el || !el.nodeName) {
      return;
    }

    while (el.firstChild) {
      cleanHandlers(el.firstChild);
      el.removeChild(el.firstChild);
    }

    return el;
  };

  var replaceWith = function (oldEl, newEls) {
    if (Element.prototype.replaceWith) {
      return oldEl.replaceWith.apply(oldEl, newEls);
    }
    var parentEl = oldEl.parentNode;
    var len = newEls.length;

    for (var i = 0; i < len; i++) {
      var currentEl = typeof newEls[i] === 'string' ? document.createTextNode(newEls[i]) : newEls[i];

      if (currentEl.parentNode) {
        currentEl.parentNode.removeChild(currentEl);
      }

      parentEl.insertBefore(currentEl, oldEl);
    }
    parentEl.removeChild(oldEl);
  };

  /**
   * Replace a DOM element with one or more other elements
   * @function replace
   * @param {Element} oldEl The element to be replaced
   * @param {Element|Array<Element>} replacement An element, or an array of elements, to insert in place of `oldEl`
   */
  var replace = function (oldEl, replacement) {

    if (!oldEl || !oldEl.parentNode) {
      return null;
    }
    if (!replacement) {
      return remove(oldEl);
    }

    cleanHandlers(oldEl);

    var newStuff = typeof replacement === 'string' ? document.createTextNode(replacement) : replacement;

    return isArray(newStuff) ? replaceWith(oldEl, newStuff) : oldEl.parentNode.replaceChild(newStuff, oldEl);
  };

  /**
   * Insert a script into the DOM with reasonable default properties, returning a promise. If `options.id` is set, will avoid loading  script if the id is already in the DOM.
   * @function loadScript
   * @param {Object} options An object of options for loading the script. All except `complete` and `completeDelay` will be set as properties on the script element before it is inserted.
   * @param {string} [options.src] The value of the script's `src` property. Required if `options.textContent` not set
   * @param {string} [options.textContent] The text content of the script. Ignored if `options.src` set. Required if `options.src` NOT set.
   * @param {boolean} [options.async=true] The value of the script's `async` property. Default is `true`.
   * @param {number} [options.completeDelay=0] Number of milliseconds to wait when the script has loaded before resolving the Promise to account for time it might take for the script to be parsed
   * @param {string} [options.id] String representing a valid identifier to set as the script element's `id` property. If set, the script will not be loaded if an element with the same id already appears in the DOM
   * @param {string} [options.onDuplicateId = resolve] One of 'resolve' or 'reject'. Whether to return a resolved or rejected promise when a script with an id matching the provided `options.id` is already in the DOM. Either way, the function will not attempt to load the script again and the resolved/rejected promise will be passed an object with `{duplicate: true}`.
   * @param {boolean|string} [...options[scriptProperties]] Any other values to be set as properties of the script element
   * @returns {Promise} Promise that is either resolved or rejected. If `options.id` is NOT provided or if no element exists with id of `options.id`, promise is resolved when script is loaded. If `options.id` IS provided and element with same id exists, promise is resolved or rejected (depending on `options.onDuplicateId`) with no attempt to load new script.
   *
   */
  var loadScript = function(options) {
    if ( options === void 0 ) options = {async: true, complete: function () {/* empty */}};

    var settings = Object.assign({
      async: true,
      id: ("s-" + (new Date().getTime())),
    }, options);
    var onDuplicateId = settings.onDuplicateId;
    var completeDelay = settings.completeDelay; if ( completeDelay === void 0 ) completeDelay = 10;
    var rest = objectWithoutProperties( settings, ["onDuplicateId", "completeDelay"] );
    var props = rest;

    if (!props.src && !props.textContent) {
      return Promise.reject(new Error('Either src or textContent must be supplied as property of options parameter'));
    }

    var script = document.createElement('script');
    var script0 = document.getElementsByTagName('script')[0];
    var done = false;

    if (document.getElementById(props.id)) {
      var promiseMethod = ['resolve', 'reject'].find(function (m) { return m === onDuplicateId; }) || 'resolve';

      script = script0 = null;

      return Promise[promiseMethod]({duplicate: true});
    }

    Object.keys(props).forEach(function (key) {
      script[key] = props[key];
    });

    // NOTE: script may not be parsed by the time the promise resolves.
    return new Promise(function (resolve, reject) {
      script.onload = script.onreadystatechange = function() {
        if (!done &&
            (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')
        ) {
          done = true;
          window.setTimeout(resolve, completeDelay);
          script.onload = script.onreadystatechange = null;
        }

      };

      if (!document.getElementById(props.id)) {
        script0.parentNode.insertBefore(script, script0);
      }
    });
  };

  var doms = /*#__PURE__*/Object.freeze({
    __proto__: null,
    toNodes: toNodes,
    $: $,
    $1: $1,
    addClass: addClass,
    removeClass: removeClass,
    toggleClass: toggleClass,
    replaceClass: replaceClass,
    getOffset: getOffset,
    setStyles: setStyles,
    setAttrs: setAttrs,
    getAttrs: getAttrs,
    prepend: prepend,
    append: append,
    before: before,
    after: after,
    createTree: createTree,
    remove: remove,
    empty: empty,
    replace: replace,
    loadScript: loadScript
  });

  var FM$1 = typeof window !== 'undefined' && window.FM ? Object.assign(window.FM, core$1) : core$1;

  FM$1.extend(FM$1, addElements, ajaxes, ga, arrays, events, forms, jsonp, maths, objects);
  FM$1.extend(FM$1, promises, selections, storages, strings, timers, transEvents, urls, youtubes);

  FM$1.extend(FM$1, colors, doms);

  window.FM = FM$1;

}());
