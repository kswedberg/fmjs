# Fusionary JavaScript

[![view on npm](http://img.shields.io/npm/v/fmjs.svg)](https://www.npmjs.org/package/fmjs)

This repo contains a bunch of plain JavaScript functions that we use often at Fusionary. They are mostly provided as ES6
modules, but a subset of them are also offered as CommonJS modules so they can easily be used in a node.js environment.

## Install

If you want to install fmjs via npm or yarn, go ahead:

```bash
npm install fmjs
```

```bash
yarn add fmjs
```

## ES6 Modules

If your bundler supports ES6 module tree shaking, you can do import any function like this:

```js
import {$, debounce, deepCopy} from 'fmjs';
```

(Note: For Webpack, you might need to configure it to treat fmjs as ES6)

Otherwise, for any of the [modules](#modules), you can do this:

```js
import {example1, example2} from 'fmjs/example'

example1('foo');
example2('bar');
```

or this (not recommended):

```js
import * as examples from 'fmjs/example'

examples.example1('foo');
examples.example2('bar');
```



## CommonJS Modules

The following [modules](#modules) &amp; their corresponding functions can be used in a node.js environment:

* array
* color
* math
* object
* promise
* string
* timer
* url

You can require them from their respective files in the `cjs` directory, like so:

```js
const {example1} = require('fmjs/cjs/example');

example1('foo');
```

or like so:

```js
const examples = require('fmjs/cjs/example');

examples.example1('foo');
```

<a name="modules"></a>
---
